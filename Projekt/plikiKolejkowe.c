#include "plikiKolejkowe_naglowek.h"
#include "generator_naglowek.h"

Kolejki strumienie;

void otworzPlikiKolejkowe(char nazwa[], char id)
{
  //otwarcie plików kolejkowych
  //i ich stworzenie w razie potrzeby
  char sciezka1[DLUGOSC_NAZWY_SCIEZKI];
  char sciezka2[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(sciezka1,"./");
  strcat(sciezka1,nazwa);

  strcpy(sciezka2,sciezka1);

  strcat(sciezka1,"/AdoB");
  strcat(sciezka2,"/BdoA");

  if(access(sciezka1,F_OK)!=0)
  {
    mkfifo(sciezka1,0777);
  }

  if(access(sciezka2,F_OK)!=0)
  {
    mkfifo(sciezka2,0777);
  }

  if(id=='A')
  {
    strumienie.wejscie=fopen(sciezka2,"r+");
    strumienie.wyjscie=fopen(sciezka1,"w+");
  }
  else
  {
    strumienie.wejscie=fopen(sciezka1,"r+");
    strumienie.wyjscie=fopen(sciezka2,"w+");
  }

  int flagi, numerPliku;
  numerPliku=fileno(strumienie.wejscie);
  flagi=fcntl(numerPliku,F_GETFL);
  fcntl(numerPliku,F_SETFL,flagi | O_NONBLOCK);

  numerPliku=fileno(strumienie.wyjscie);
  flagi=fcntl(numerPliku,F_GETFL);
  fcntl(numerPliku,F_SETFL,flagi | O_NONBLOCK);

  if(strumienie.wejscie == NULL || strumienie.wyjscie==NULL)
  {
    fprintf(stderr,"Błąd w odczycie plików kolejkowych.\n");
    return;
  }
  return;
}

void zamknijPlikiKolejkowe()
{
  fclose(strumienie.wejscie);
  fclose(strumienie.wyjscie);
  return;
}

void wypiszWiadomoscDoPlikuKolejkowego(char wiadomosc[DLUGOSC_WIADOMOSCI])
{
  //wypisanie wiadomości do strumienia wyjściowego
//  fprintf(stderr,"Wypisano wiadomość: %s\n",wiadomosc);
  fprintf(strumienie.wyjscie,"%s",wiadomosc);
  fflush(strumienie.wyjscie);
  return;
}

void wczytajWiadomosc(char wiadomosc[DLUGOSC_WIADOMOSCI])
{
  //pobranie wiadomości ze strumienia wejściowego
  fgets(wiadomosc,DLUGOSC_WIADOMOSCI,strumienie.wejscie);
  fflush(strumienie.wejscie);
  return;
}
