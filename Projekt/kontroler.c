#include "okienka_naglowek.h"
#include "generator_naglowek.h"

#define DLUGOSC_WIERSZA_MGLY 100000

void stworzTabliceEnergiiDoWejsciaNaPola(int energia[LICZBA_SYMBOLI])
{
  //tworzy tablicę przechowującą koszty PE które trzeba ponieść
  //aby wejść na pole o danym id
  energia[0]=INF; //blokada
  energia[1]=1;   //łąka
  energia[2]=2;   //las
  energia[3]=3;   //góry
  energia[4]=2;   //maliny
  energia[5]=3;   //wieża
  energia[6]=2;   //barykada
  return;
}

char MojID(char *znak)
{
  //id naszej kopii gry
  static char moj_id;
  if(znak!=NULL)
  {
    moj_id=*znak;
  }
  return moj_id;
}

char TwojID(char *znak)
{
  //id kopii gry przeciwnika
  static char twoj_id;
  if(znak!=NULL)
  {
    twoj_id=*znak;
  }
  return twoj_id;
}

int KolumnyWMapie(int *m)
{
  //liczba kolumn we wczytanej mapie
  static int kolumny;
  if(m!=NULL)
  {
    kolumny=*m;
  }
  return kolumny;
}

int WierszeWMapie(int *wsk)
{
  //liczba wierszy we wczytanej mapie
    static int n;
    if(wsk!=NULL)
    {
      n=*wsk;
    }
    return n;
}

GtkWidget* odnosnikDoOknaProgramu(GtkWidget *okno)
{
  //wskaźnik na okno programu
  //jeśli null zwaraca zapamiętany wskaźnik na okno główne programu
  static GtkWidget *oknoProgramu;
  if(okno!=NULL)
  {
    oknoProgramu=okno;
  }
  return oknoProgramu;
}

int ustawPodlaczenieDoMapy(char nazwa[],char id, GtkWidget *oknoProgramu)
{
  //funkcja sprawdzająca czy można podłączyć gracza
  //do mapy i jeśli tak to robiąca to
  char a,b;

  if(id!='A' && id!='B')
  {
    fprintf(stderr,"Błędne id:%c\n",id);
    wyswietlBlad(oknoProgramu,"Podano błędny typ gracza.");
    return -1;
  }

  char sciezka[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(sciezka,"./");
  strcat(sciezka,nazwa);
  strcat(sciezka,"/kontrolerMapy");

  FILE *kontrolerMapy = fopen(sciezka,"r+");
  if(kontrolerMapy==NULL)
  {
    fprintf(stderr,"Błąd dostępu do kontrolera mapy '%s' w funkcji ustawPodlaczenieDoMapy\n",
            sciezka);
    return -1;
  }
  fscanf(kontrolerMapy,"Gracze podłączeni:%c%c\n",&a,&b);
  if(id=='A')
  {
    if(a=='x')
    {
      a=id;
    }
    else
    {
      fprintf(stderr,"Błąd podłączania gracza %c do mapy. Wartość w polu: %c\n",
              id,a);
      wyswietlBlad(oknoProgramu,"Błąd przy dołączaniu do gry. Proszę sprawdzić \
czy dwóch graczy nie wybrało tego samego typu gracza.");
      return -1;
    }
  }
  else
  {
    if(b=='x')
    {
      b=id;
    }
    else
    {
      fprintf(stderr,"Błąd podłączania gracza %c do mapy. Wartość w polu: %c\n",
              id,b);
      wyswietlBlad(oknoProgramu,"Błąd przy dołączaniu do gry. Proszę sprawdzić \
czy dwóch graczy nie wybrało tego samego typu gracza.");
      return -1;
    }
  }

  rewind(kontrolerMapy);
  fprintf(kontrolerMapy,"Gracze podłączeni:%c%c\n",a,b);

  fclose(kontrolerMapy);

  return 0;
}

void odlaczGraczaOdMapy(char nazwa[], char id)
{
  //funkcja odłączająca gracza od mapy przy wychodzeniu z gry
  char a,b;

  char sciezka[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(sciezka,"./");
  strcat(sciezka,nazwa);
  strcat(sciezka,"/kontrolerMapy");

  FILE *kontrolerMapy = fopen(sciezka,"r+");
  if(kontrolerMapy==NULL)
  {
    fprintf(stderr,"Błąd otwarcia kontrolera mapy:'%s' w odlaczGraczaOdMapy.\n",
            sciezka);
    return;
  }

  fscanf(kontrolerMapy,"Gracze podłączeni:%c%c\n",&a,&b);

  if(id=='A'){
    a='x';
  }
  else{
    b='x';
  }

  rewind(kontrolerMapy);
  fprintf(kontrolerMapy,"Gracze podłączeni:%c%c\n",a,b);

  fclose(kontrolerMapy);
  return;
}

int* przydzielPamiecNaTabliceMapy(int n, int m)
{
  int *wsk=(int*)malloc(sizeof(int)*n*m);
  return wsk;
}

int* MapaWTablicy(int *pam)
{
  //mapa gry zapisana w postaci jednowymiarowej tablicy
  //zwraca wskaźnik na mapę, jeśli pam jest różne od NULL
  //to aktualizuje wskaźnik
  static int *mapaTab;
  if(pam!=NULL)
  {
    mapaTab=pam;
  }
  return mapaTab;
}

char* odnosnikDoNazwyMapy(char nazwa[])
{
  static char nazwaMapy[DLUGOSC_NAZWY_SCIEZKI];
  if(nazwa!=NULL)
  {
    strcpy(nazwaMapy,nazwa);
  }
  return nazwaMapy;
}

Pozycja pozycjaGracza(Pozycja *gracz, char id)
{
  static Pozycja gracz1;
  static Pozycja gracz2;
  if(id=='A')
  {
    if(gracz!=NULL)
    {
      gracz1.x=gracz->x;
      gracz1.y=gracz->y;
    }
    return gracz1;
  }
  else
  {
    if(gracz!=NULL)
    {
      gracz2.x=gracz->x;
      gracz2.y=gracz->y;
    }
    return gracz2;
  }
}

void pobierzZPlikowPozycjeGraczy()
{
  //funkcja pobierająca z plików pozycje gracza A i B
  char sciezka[DLUGOSC_NAZWY_SCIEZKI];
  char sciezka2[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(sciezka,"./");
  strcat(sciezka,odnosnikDoNazwyMapy(NULL));
  strcpy(sciezka2,sciezka);

  strcat(sciezka,"/graczA");
  strcat(sciezka2,"/graczB");

//  fprintf(stderr,"Scieżka: %s\n",sciezka);

  FILE *plikGraczaA = fopen(sciezka,"r+");
  FILE *plikGraczaB = fopen(sciezka2,"r+");

  if(plikGraczaA==NULL || plikGraczaB==NULL)
  {
    wyswietlBlad(odnosnikDoOknaProgramu(NULL),
                 "Błąd w pobieraniu pozycji graczy z kontrolera.");
    return;
  }
  int x,y;
  Pozycja gracz;

  fscanf(plikGraczaA,"Pozycja:%d %d\n",&x,&y);
  gracz.x=x;
  gracz.y=y;
  pozycjaGracza(&gracz,'A');

  fscanf(plikGraczaB,"Pozycja:%d %d\n",&x,&y);
  gracz.x=x;
  gracz.y=y;
  pozycjaGracza(&gracz,'B');

  fclose(plikGraczaA);
  fclose(plikGraczaB);
  return;
}

Gracz InfoOMnie(Gracz *ja)
{
  static Gracz Ja;
  if(ja!=NULL)
  {
    Ja=*ja;
  }
  return Ja;
}

void UstawDomyslneStatystykiGracza()
{
  //funkcja ustawiająca domyślne wartości dla parametrów gracza
  //umożliwia to późniejszy rozwój programu
  //i tworzenie zapamiętywalnych profili graczy o
  //penych (wyższych niż standardowe) statystykach
  Gracz ja;
  ja.maxPE=5;
  ja.maxPZ=100;
  ja.maliny=0;
  InfoOMnie(&ja);
  return;
}

int PunktZycia(int *punkty, char id)
{
  static int PZA, PZB;
  if(punkty!=NULL)
  {
    if(id=='A')
      PZA=*punkty;
    if(id=='B')
      PZB=*punkty;
    if(id!='A' && id!='B')
      fprintf(stderr, "Błąd w otrzymanym id. Funkcja: PunktyZycia.\n");
  }
  if(id=='A')
    return PZA;
  else
    return PZB;
}

int PunktyEnergii(int *punkty, char id)
{
  static int PEA, PEB;
  if(punkty!=NULL)
  {
    if(id=='A')
      PEA=*punkty;
    if(id=='B')
      PEB=*punkty;
    if(id!='A' && id!='B')
      fprintf(stderr, "Błąd w otrzymanym id. Funkcja: PunktyEnergii.\n");
  }
  if(id=='A')
    return PEA;
  else
    return PEB;
}

void resetujPunktyEnergii()
{
  //przywracanie punktów energii gracza do maksymalnego poziomu
  Gracz ja = InfoOMnie(NULL);

  int PE=ja.maxPE;
  PunktyEnergii(&PE,MojID(NULL));

  wyswietlStatystykiGracza();

  return;
}

void resetujPunktyZycia()
{
  //przywracanie punktów życia do maksymalnego poziomu
  Gracz ja=InfoOMnie(NULL);
  int PZ=ja.maxPZ;
  PunktZycia(&PZ,MojID(NULL));
  wyswietlStatystykiGracza();
  return;
}

GtkWidget* odnosnikDoNapisuZeStatystykami(GtkWidget **napis)
{
  static GtkWidget *statystyki;
  if(napis!=NULL)
  {
    statystyki=*napis;
  }
  return statystyki;
}

void zaladujMgleWojny(char id, char nazwa[],int n, int m)
{
  //wczytywanie mgły wojny danego gracza z pliku
  char sciezka[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(sciezka,"./");
  strcat(sciezka,nazwa);
  strcat(sciezka,"/gracz");
  if(id=='A')
  {
    strcat(sciezka,"A");
  }
  else
  {
    strcat(sciezka,"B");
  }

  FILE *plik=fopen(sciezka,"r");
  if(plik==NULL)
  {
    wyswietlBlad(odnosnikDoOknaProgramu(NULL),
                "Błąd otwarcia pliku w funkcji zaladujMgleWojny.");
    fprintf(stderr,"Błąd otwarcia pliku w funkcji zaladujMgleWojny. Scieżka:%s",
            sciezka);
    return;
  }

  int *mgla=przydzielPamiecNaMgleWojny(n,m);

  char znak=fgetc(plik);
  char znak2=fgetc(plik);
  while(znak!='M' || znak2!='g')
  {
    znak=znak2;
    znak2=fgetc(plik);
  }
  fseek(plik,-2,1);

 // fprintf(stderr,"!!%ld!!",ftell(plik));
  fscanf(plik,"Mgla:<%d>",&mgla[0]);
 // fprintf(stderr,"!!%ld!!",ftell(plik));
 // fprintf(stderr,"%d ",mgla[0]);
  for(int i=1;i<n*m;i++)
  {
    fscanf(plik,"<%d>",&mgla[i]);
 //   fprintf(stderr,"%d ",mgla[i]);
  }
  fscanf(plik,"\n");

  mglaWojny(mgla);

  fclose(plik);
  return;
}

void zapiszMgleWojny(char id, int n, int m)
{
  //zapisywanie mgły wojny danego gracza do pliku
  char *nazwa=odnosnikDoNazwyMapy(NULL);
  char sciezka[DLUGOSC_NAZWY_SCIEZKI];

  strcpy(sciezka,"./");
  strcat(sciezka,nazwa);
  strcat(sciezka,"/gracz");
  if(id=='A')
  {
    strcat(sciezka,"A");
  }
  else
  {
    strcat(sciezka,"B");
  }

  FILE *plik=fopen(sciezka,"r+");
  if(plik==NULL)
  {
    wyswietlBlad(odnosnikDoOknaProgramu(NULL),
                "Błąd otwarcia pliku w funkcji zapiszMgleWojny.");
    fprintf(stderr,"Błąd otwarcia pliku w funkcji zapiszMgleWojny. Scieżka:%s",
            sciezka);
    return;
  }

  int *mgla=mglaWojny(NULL);

  char pomoc[DLUGOSC_WIADOMOSCI];
  fgets(pomoc,INF,plik);
  fgets(pomoc,INF,plik);
  fgets(pomoc,INF,plik);
  fgets(pomoc,INF,plik);

  fprintf(plik,"Mgla:");
  for(int i=0;i<n*m;i++)
  {
    fprintf(plik,"<%d>",mgla[i]);
  }
  fprintf(plik,"\n");

  fclose(plik);
  return;
}

void zaladujParametryGracza(char id, char nazwa[])
{
  //wczytywanie parametrów gracza takich jak PE, maxPE i PZ
  char sciezka[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(sciezka,"./");
  strcat(sciezka,nazwa);
  strcat(sciezka,"/gracz");
  if(id=='A')
  {
    strcat(sciezka,"A");
  }
  else
  {
    strcat(sciezka,"B");
  }

  FILE *plik=fopen(sciezka,"r");
  if(plik==NULL)
  {
    wyswietlBlad(odnosnikDoOknaProgramu(NULL),
                "Błąd otwarcia pliku w funkcji zaladujParametryGracza.");
    fprintf(stderr,"Błąd otwarcia pliku w funkcji zaladujParametryGracza. Scieżka:%s",
            sciezka);
    return;
  }

  int PE,PZ;
  int p1,p2;
  int maliny;
  fscanf(plik,"Pozycja:%d %d\n",&p1, &p2);
  fscanf(plik,"PŻ:%d\n",&PZ);
  fscanf(plik,"PE:%d\n",&PE);

  char znak=fgetc(plik);
  char znak2=fgetc(plik);
  while(znak!='M' || znak2!='a')
  {
    znak=znak2;
    znak2=fgetc(plik);
  }
  fseek(plik,-2,1);

  fscanf(plik,"Maliny:%d\n",&maliny);

  Gracz ja=InfoOMnie(NULL);
  if(id==MojID(NULL))
  {
    ja.maliny=maliny;
    InfoOMnie(&ja);
  }
  if(PZ>ja.maxPZ)
  {
    PZ=ja.maxPZ;
  }
  if(PE>ja.maxPE)
  {
    PE=ja.maxPE;
  }
  PunktZycia(&PZ,id);
  PunktyEnergii(&PE,id);
  fclose(plik);
  return;
}

void zapiszParametryGracza(char id)
{
  //zapisywanie parametrów gracza takich jak PE, maxPE i PZ
  char *nazwa=odnosnikDoNazwyMapy(NULL);
  char sciezka[DLUGOSC_NAZWY_SCIEZKI];
  Pozycja gracz=pozycjaGracza(NULL,id);

  strcpy(sciezka,"./");
  strcat(sciezka,nazwa);
  strcat(sciezka,"/gracz");
  if(id=='A')
  {
    strcat(sciezka,"A");
  }
  else
  {
    strcat(sciezka,"B");
  }

  FILE *plik=fopen(sciezka,"r+");
  if(plik==NULL)
  {
    wyswietlBlad(odnosnikDoOknaProgramu(NULL),
                "Błąd otwarcia pliku w funkcji zapiszParametryGracza.");
    fprintf(stderr,"Błąd otwarcia pliku w funkcji zapiszParametryGracza. Scieżka:%s",
            sciezka);
    return;
  }

  fprintf(plik,"Pozycja:%d %d\n",gracz.x, gracz.y);
  fprintf(plik,"PŻ:%d\n",PunktZycia(NULL,id));
  fprintf(plik,"PE:%d\n",PunktyEnergii(NULL,id));
  if(id==MojID(NULL))
  {
    Gracz ja=InfoOMnie(NULL);
    fprintf(plik,"Maliny:%d\n",ja.maliny);
  }
  fclose(plik);
  return;
}

Smok* przydzielPamiecNaTabliceSmokow(int S)
{
  Smok *Stab=(Smok*)malloc(sizeof(Smok)*S);
  return Stab;
}

Smok* TablicaZeSmokami(Smok *wsk)
{
  //tablica opisująca smoki
  static Smok *Stab;
  if(wsk!=NULL)
  {
    Stab=wsk;
  }
  return Stab;
}

int liczbaSmokow(int *smoki)
{
  static int S;
  if(smoki!=NULL)
  {
    S=*smoki;
  }
  return S;
}

int* przydzielPamiecNaTabliceObecnosciSmokow(int n, int m)
{
  int *wsk=(int*)calloc(n*m,sizeof(int));
  return wsk;
}

int* TablicaObecnosciSmokow(int *wsk)
{
  //tablica zapamiętująca czy na polu o współrzędnych x,y
  //znajduje się ZSP jeśli tak to w tej tablicy
  //w komórce x*m+y będzie znajdowała się 1
  static int *SOtab;
  if(wsk!=NULL)
  {
    SOtab=wsk;
  }
  return SOtab;
}

static void resetujPolaSmokow()
{
  //funkcja przywracająca pola na których stały smoki do
  //stanu naturalnego, (otrzymanego z generatora)
  Smok *Stab=TablicaZeSmokami(NULL);
  int S=liczbaSmokow(NULL);
  int m=KolumnyWMapie(NULL);
  for(int i=0;i<S;i++)
  {
    przywrocPoleTerenu(Stab[i].pozycja,m);
  }
  return;
}

void resetujPolaGraczy()
{
  Pozycja graczA=pozycjaGracza(NULL,'A');
  Pozycja graczB=pozycjaGracza(NULL,'B');
  int m=KolumnyWMapie(NULL);
  przywrocPoleTerenu(graczA,m);
  przywrocPoleTerenu(graczB,m);
  return;
}

void mojaTura()
{
  //funkcja wywoływana na początku tury, po
  //otrzymaniu informacji o zakończeniu tury przez drugiego gracza
  //pobiera dane i umożliwia graczowi wykonanie akcji
  resetujPolaSmokow();
  resetujPolaGraczy();
  free(TablicaZeSmokami(NULL));
  free(mglaWojny(NULL));
  pobierzSmokiZPliku(odnosnikDoNazwyMapy(NULL),WierszeWMapie(NULL),
                     KolumnyWMapie(NULL));
  pobierzZPlikowPozycjeGraczy();
  zaladujAktualnaTure(odnosnikDoNazwyMapy(NULL));
  zaladujParametryGracza(MojID(NULL),odnosnikDoNazwyMapy(NULL));
  zaladujParametryGracza(TwojID(NULL),odnosnikDoNazwyMapy(NULL));
  zaladujMgleWojny(MojID(NULL), odnosnikDoNazwyMapy(NULL),WierszeWMapie(NULL),
                   KolumnyWMapie(NULL));
  wyswietlSmokiNaPoczatku();
  if(1==sprawdzCzyGraczZginal(MojID(NULL)) || 1==sprawdzCzyGraczZginal(TwojID(NULL)))
  {
    return;
  }
  wyswietlStatystykiGracza();
  ustawGraczaNaMapie(pozycjaGracza(NULL,MojID(NULL)),MojID(NULL));
  ustawGraczaNaMapie(pozycjaGracza(NULL,TwojID(NULL)),TwojID(NULL));
  return;
}

int petlaStop(int *wsk)
{
  //informacja o konieczności przerwania sprawdzania
  //plików kolejkowych w oczekiwaniu na nowe wiadomości
  //jeśli należy przerwać to stop==1
  static int stop;
  if(wsk!=NULL)
  {
    stop=*wsk;
  }
  return stop;
}

gboolean sprawdzanieNadszlychWiadomosci()
{
  //funkcja pobierająca wiadomości z plików kolejkowych i je
  //przetwarzająca
  if(petlaStop(NULL)==1)
  {
    return FALSE;
  }
  char wiadomosc[DLUGOSC_WIADOMOSCI];
  wczytajWiadomosc(wiadomosc);
//  fprintf(stderr,"Wczytana wiadomość:%s\n", wiadomosc);
  char tekst[DLUGOSC_WIADOMOSCI];
  sprintf(tekst,"Twoja kolej %c.",MojID(NULL));
  if(strcmp(tekst,wiadomosc)==0)
  {
//    fprintf(stderr,"Wiadomość zgadza się ze wzorcem. Kopia: %c\n",MojID(NULL));
    mojaTura();
    return TRUE;
  }
  if(strcmp(wiadomosc,"Wygrałeś!")==0)
  {
    wygranaGracza(MojID(NULL),TwojID(NULL));
    return FALSE;
  }
  if(strcmp(wiadomosc,"Przegrałeś!")==0)
  {
    wygranaGracza(TwojID(NULL),MojID(NULL));
    return FALSE;
  }
  return TRUE;
}

int stanAtaku(int *wsk)
{
  //informacja czy przycisk ataku jest aktualnie wciśnięty
  //1 jeśli tak 0 wpp
  static int stan;
  if(wsk!=NULL)
  {
    stan=*wsk;
  }
  return stan;
}

void wygranaGracza(char idWygranego, char idPrzegranego)
{
  //funkcja kończąca grę po wygraniu któregoś z graczy,
  //nakazuje wyświetlenie komunikatów wygranej/przegranej
  char wiadomosc[DLUGOSC_WIADOMOSCI];
  if(MojID(NULL)==idWygranego)
  {
    sprintf(wiadomosc,"Przegrałeś!");
    wypiszWiadomoscDoPlikuKolejkowego(wiadomosc);
    wyswietlOkienkoWygranej("Gratulacje!\nWygrałeś grę!");
    wrocDoMenuGlownego(NULL,odnosnikDoOknaProgramu(NULL));
    return;
  }
  else
  {
    sprintf(wiadomosc,"Wygrałeś!");
    wypiszWiadomoscDoPlikuKolejkowego(wiadomosc);
    wyswietlOkienkoWygranej("Niestety przegrałeś.\nSpróbuj może jeszcze raz.");
    wrocDoMenuGlownego(NULL,odnosnikDoOknaProgramu(NULL));
    return;
  }
}

char aktualnaTura(char *id)
{
  //informacja kogo jest aktualnie tura
  static char tura;
  if(id!=NULL)
  {
    tura=*id;
  }
  return tura;
}

void zaladujAktualnaTure(char nazwa[])
{
  //pobieranie informacji o aktualnej turze z pliku
  char sciezka[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(sciezka,"./");
  strcat(sciezka,nazwa);
  strcat(sciezka,"/kontrolerMapy");

  FILE *kontrolerMapy = fopen(sciezka,"r");
  if(kontrolerMapy==NULL)
  {
    fprintf(stderr,"Błąd otwarcia kontrolera mapy w funkcji: zaladujAktualnaTura.\n");
    wyswietlBlad(odnosnikDoOknaProgramu(NULL), "Błąd otwarcia pliku.");
    return;
  }

  char p1,p2;
  fscanf(kontrolerMapy,"Gracze podłączeni:%c%c\n",&p1,&p2);
  char tura;
  fscanf(kontrolerMapy,"Aktualna tura:%c\n",&tura);

  aktualnaTura(&tura);

  fclose(kontrolerMapy);
  return;
}

void zapiszAktualnaTura()
{
  //zapisywanie informacji o aktualnej turze do pliku
  char sciezka[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(sciezka,"./");
  strcat(sciezka,odnosnikDoNazwyMapy(NULL));
  strcat(sciezka,"/kontrolerMapy");

  FILE *kontrolerMapy=fopen(sciezka,"r+");
  if(kontrolerMapy==NULL)
  {
    fprintf(stderr,"Błąd otwarcia kontrolera mapy w funkcji: zapiszAktualnaTura.\n");
    wyswietlBlad(odnosnikDoOknaProgramu(NULL), "Błąd otwarcia pliku.");
    return;
  }

  char p1,p2;
  fscanf(kontrolerMapy,"Gracze podłączeni:%c%c\n",&p1,&p2);
  rewind(kontrolerMapy);

  fprintf(kontrolerMapy,"Gracze podłączeni:%c%c\n",p1,p2);
  fprintf(kontrolerMapy,"Aktualna tura:%c\n", aktualnaTura(NULL));

  fclose(kontrolerMapy);
  return;
}

int* przydzielPamiecNaMgleWojny(int n, int m)
{
  return (int*)calloc(n*m,sizeof(int));
}

int* mglaWojny(int *wsk)
{
  static int *mgla;
  if(wsk!=NULL)
  {
    mgla=wsk;
  }
  return mgla;
}

