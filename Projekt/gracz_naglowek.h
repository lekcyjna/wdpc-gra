#ifndef GRACZ_H_INCLUDED
#define GRACZ_H_INCLUDED

#include "generator_naglowek.h"
#include "kontroler_naglowek.h"

void WPrawo();
void WLewo();
void WGore();
void WDol();
void uzdrowGracza(double procent,char id);
int sprawdzCzyGraczZginal(char id);

#endif // GRACZ_H_INCLUDED
