#ifndef OKIENKA_NAGLOWEK_H_INCLUDED
#define OKIENKA_NAGLOWEK_H_INCLUDED

#include<stdlib.h>
#include<gtk/gtk.h>
//#include<gdk-pixbuf/gdk-pixbuf.h>

#include "generator_naglowek.h"
#include "kontroler_naglowek.h"
#include "plikiKolejkowe_naglowek.h"
#include "gracz_naglowek.h"
#include "smoki_naglowek.h"

GtkWidget* stworzOknoGlowne();
void wyswietlBlad(GtkWidget *oknoProgramu, char komunikat[]);
void ustawGraczaNaMapie(Pozycja gracz, char id);
void przywrocPoleTerenu(Pozycja pole, int m);
void wyswietlStatystykiGracza();
void ustawSmokaNaMapie(Pozycja smok);
void pobierzSmokiZPliku(char nazwa[], int n, int m);
void wyswietlSmokiNaPoczatku();
void deaktywujPrzyciskAtaku(GtkWidget *wsk);
void wyswietlStatystykiGraczaWrazZObrazeniami(int obrazenia);
void wrocDoMenuGlownego(GtkWidget *ojciec, GtkWidget *oknoProgramu);
void wyswietlOkienkoWygranej(char wiadomosc[DLUGOSC_WIADOMOSCI]);

#endif // OKIENKA_NAGLOWEK_H_INCLUDED
