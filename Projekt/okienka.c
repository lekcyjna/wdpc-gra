#include "okienka_naglowek.h"

#define ODSTEP_MIEDZY_PRZYCISKAMI 10
#define PODSTAWOWA_SZEROKOSC_OKNA 900
#define PODSTAWOWA_WYSOKOSC_OKNA 600
#define SZEROKOSC_MARGINESOW 150
#define LICZBA_LINI_STATYSTYK 3
#define MAX_LICZBA_ZNAKOW_W_LINII_STATYSTYK 50
#define ODSTEPY_W_MAPIE 2
#define LICZBA_CYFR 10
#define SZEROKOSC_PRAWEJ_CZESCI_EKRANU 240
#define SZEROKOC_PRZYCISKOW_W_MENU_GLOWNYM 250
#define ODSTEP_CZASOWY_MIEDZY_SPRAWDZENIEM_DANYCH 100
#define MAXYMALNA_LICZBA_ZNAKOW_W_OKNACH_POPUP 60
#define SZEROKOSC_OKNA_WIADOMOSCI_POPUP 300
#define WYSOKOSC_OKNA_WIADOMOSCI_POPUP 10

static void stworzMenuGlowne(GtkWidget *oknoProgramu);
static void wyjdzZGryBedacWEkranieGry();
static void oknoTrybuDebug();

static void stworzSciezkiDoObrazkowGraczy(char gracz1[], char gracz2[])
{
  //ścieżki do obrazków graczy
  strcpy(gracz1,"./dane/Gracz1.png");
  strcpy(gracz2,"./dane/Gracz2.png");
  return;
}

static void stworzTabliceSciezekDoObrazowPol(char sciezki[][DLUGOSC_NAZWY_SCIEZKI])
{
  //ścieżki do obrazków pól
  for(int i=0;i<LICZBA_SYMBOLI;i++)
  {
    strcpy(sciezki[i],"./dane/Pole-");
  }
  strcat(sciezki[0],"blokady.png");
  strcat(sciezki[1],"trawa.png");
  strcat(sciezki[2],"las.png");
  strcat(sciezki[3],"gory.png");
  strcat(sciezki[4],"malin.png");
  strcat(sciezki[5],"wiezy.png");
  strcat(sciezki[6],"barykady.png");
  return;
}

static void stworzSciezkeDoObrazkuMgly(char sciezka[])
{
  //scieżka do obrazku mgły wojny
  strcpy(sciezka,"./dane/Mgla-wojny.png");
  return;
}

static void stworzSciezkeDoObazkuSmoka(char sciezka[])
{
  //ścieżka do obrazku smoka
  strcpy(sciezka,"./dane/Smok.png");
  return;
}

static void wczytajMapeDoTablicy(int n, int m,char nazwa[DLUGOSC_NAZWY_SCIEZKI], int tab[n][m])
{
  //funkcja pobierająca mapę z pliku i wczytująca ją do tablicy
  char sciezka[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(sciezka,"./");
  strcat(sciezka,nazwa);
  if(access(sciezka,F_OK)!=0)
  {
    fprintf(stderr,"Błąd otwarcia katalogu z mapą.\n");
  }
  strcat(sciezka,"/mapa");
  FILE *plik=fopen(sciezka,"r");

  int *mapaTab=MapaWTablicy(przydzielPamiecNaTabliceMapy(n,m));

  if(plik==NULL)
  {
    fprintf(stderr,"Błąd w odczycie mapy.\n");
  }
  for(int i=0;i<n;i++)
  {
    for(int j=0;j<m;j++)
    {
      fscanf(plik,"<%d>",&tab[i][j]);
      mapaTab[i*m+j]=tab[i][j];
    }
    getc(plik);
  }
  fclose(plik);
  return;
}

static void wypelnijSiatkeMapy(int n, int m, char nazwa[DLUGOSC_NAZWY_SCIEZKI],
                        GtkWidget *siatka)
{
  //pobieranie mapy do tablicy i wstawianie na podstawie
  //id odpowiednich obrazków w pola siatki
  int mapaTab[n][m];
  int *mgla=mglaWojny(NULL);
  char sciezki[LICZBA_SYMBOLI][DLUGOSC_NAZWY_SCIEZKI];
  char sciezkaMgla[DLUGOSC_NAZWY_SCIEZKI];
  stworzSciezkeDoObrazkuMgly(sciezkaMgla);
  stworzTabliceSciezekDoObrazowPol(sciezki);
  wczytajMapeDoTablicy(n,m,nazwa,mapaTab);
//  wypiszTablice(n,m,mapaTab);
  for(int i=0;i<n;i++)
  {
    for(int j=0;j<m;j++)
    {
      GtkWidget *obrazek;
      if(mgla[i*m+j]==1)
      {
        obrazek=gtk_image_new_from_file(sciezki[mapaTab[i][j]]);
      }
      else
      {
        obrazek=gtk_image_new_from_file(sciezkaMgla);
      }
      if(obrazek==NULL)
      {
        fprintf(stderr,"Błąd w pobieraniu obrazka pola.\n");
        fprintf(stderr,"Współrzędne w tablicy: i:%d, j:%d\n",i,j);
        fprintf(stderr,"Id pola:%d\n",mapaTab[i][j]);
      }
      //współrzędne wierszy i kolumny pobierane są przez
      //gtk_grid_attach w odwrotnej kolejności
      gtk_grid_attach(GTK_GRID(siatka),obrazek,j,i,1,1);
    }
  }
  return;
}

static void stworzLewaPoloweEkranuGry(GtkWidget *panel, int n, int m, char nazwa[])
{
  //tworzenie siatki z mapą
  GtkWidget *oknoPrzewijane=gtk_scrolled_window_new(NULL,NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(oknoPrzewijane),
                                 GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);
  gtk_paned_add1(GTK_PANED(panel),oknoPrzewijane);

  GtkWidget *siatka=gtk_grid_new();
  gtk_grid_set_column_homogeneous(GTK_GRID(siatka),TRUE);
  gtk_grid_set_row_homogeneous(GTK_GRID(siatka),TRUE);
  gtk_grid_set_column_spacing(GTK_GRID(siatka),ODSTEPY_W_MAPIE);
  gtk_grid_set_row_spacing(GTK_GRID(siatka),ODSTEPY_W_MAPIE);
  gtk_container_add(GTK_CONTAINER(oknoPrzewijane),siatka);

  wypelnijSiatkeMapy(n,m,nazwa,siatka);
  return;
}

void wrocDoMenuGlownego(GtkWidget *ojciec, GtkWidget *oknoProgramu)
{
  //funkcja zapisująca dane, zamykająca ekran gry i wracająca do
  //menu głównego
  GtkWidget *ekranGry=gtk_bin_get_child(GTK_BIN(oknoProgramu));

  int pomoc=1;
  petlaStop(&pomoc);
  zapiszMgleWojny(MojID(NULL),KolumnyWMapie(NULL),WierszeWMapie(NULL));
  int S=liczbaSmokow(NULL);
  Smok *Stab=TablicaZeSmokami(NULL);
  zapiszSmokiDoPliku(Stab,S,odnosnikDoNazwyMapy(NULL));
  zapiszParametryGracza(MojID(NULL));
  zapiszParametryGracza(TwojID(NULL));
  free(MapaWTablicy(NULL));
  free(TablicaObecnosciSmokow(NULL));
  free(TablicaZeSmokami(NULL));
  free(mglaWojny(NULL));
  odlaczGraczaOdMapy(odnosnikDoNazwyMapy(NULL),MojID(NULL));
  zapiszAktualnaTura();
  zamknijPlikiKolejkowe();

  gtk_widget_destroy(ekranGry);

  g_signal_handlers_disconnect_by_func(G_OBJECT(oknoProgramu),
                                       G_CALLBACK(wyjdzZGryBedacWEkranieGry),NULL);
  g_signal_connect(G_OBJECT(oknoProgramu),"destroy",G_CALLBACK(gtk_main_quit),NULL);

  stworzMenuGlowne(oknoProgramu);
  return;
}

static void koniecTury(GtkWidget *ojciec)
{
  //funkcja wywoływana po kliknięciu "Koniec tury"
  //zapisuje dane i przekazuje je do drugiej kopii programu
  if(MojID(NULL)!=aktualnaTura(NULL))
  {
    return;
  }
  char wiadomosc[DLUGOSC_WIADOMOSCI];
  if(1==sprawdzCzyGraczZginal(MojID(NULL)) || 1==sprawdzCzyGraczZginal(TwojID(NULL)))
  {
   return;
  }
  char pomoc=(MojID(NULL)=='A'?'B':'A');
  aktualnaTura(&pomoc);
  zapiszAktualnaTura();
  wyswietlStatystykiGracza();
  uzdrowGracza(UZDROW_KONIEC_TURY,MojID(NULL));
  resetujPunktyEnergii();
  if(aktualnaTura(NULL)=='A')
  {
    ruchySmokow();
  }
  Smok *Stab=TablicaZeSmokami(NULL);
  int S=liczbaSmokow(NULL);
  zapiszSmokiDoPliku(Stab,S,odnosnikDoNazwyMapy(NULL));
  zapiszParametryGracza('A');
  zapiszParametryGracza('B');
  zapiszMgleWojny(MojID(NULL),WierszeWMapie(NULL),KolumnyWMapie(NULL));
  sprintf(wiadomosc,"Twoja kolej %c.",TwojID(NULL));
  wypiszWiadomoscDoPlikuKolejkowego(wiadomosc);
  return;
}

static void kliknietoPrzyciskAtaku(GtkWidget *ojciec)
{
  //funkcja wywoływana przez kliknięcie przycisku "Atak"
  int pomoc;
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ojciec))==TRUE)
  {
    pomoc=1;
    stanAtaku(&pomoc);
  }
  else
  {
    pomoc=0;
    stanAtaku(&pomoc);
  }
  return;
}

void deaktywujPrzyciskAtaku(GtkWidget *wsk)
{
  //odznaczenie przycisku "Atak" po zaatakowaniu kogoś
  static GtkWidget *przycisk;
  int pomoc=0;
  if(wsk!=NULL)
  {
    przycisk=wsk;
  }
  stanAtaku(&pomoc);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(przycisk),FALSE);
  return;
}

void zjedzMaline(GtkWidget *ojciec, GtkWidget *wiersz)
{
  Gracz ja=InfoOMnie(NULL);
  if(ja.maliny<=0)
  {
    return;
  }
  uzdrowGracza(UZDROW_MALINA,MojID(NULL));
  wyswietlStatystykiGracza();
  ja.maliny--;
  InfoOMnie(&ja);

  char maliny[DLUGOSC_NAZWY_SCIEZKI];
  char pomoc[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(maliny,"<b><span color=\"red\">");
  strcat(maliny,"Maliny: ");
  sprintf(pomoc,"%d",ja.maliny);
  strcat(maliny, pomoc);
  strcat(maliny,"</span></b>");

  GtkWidget *siatka=gtk_bin_get_child(GTK_BIN(wiersz));
  GtkWidget *napis=gtk_grid_get_child_at(GTK_GRID(siatka),0,0);
  gtk_label_set_markup(GTK_LABEL(napis),maliny);
  return;
}

void wyswietlEkwipunek(GtkWidget *ojciec, GtkWidget *oknoProgramu)
{
  int malinyLiczba;
  char maliny[DLUGOSC_NAZWY_SCIEZKI];
  char pomoc[DLUGOSC_NAZWY_SCIEZKI];
  Gracz ja=InfoOMnie(NULL);
  malinyLiczba=ja.maliny;

  strcpy(maliny,"<b><span color=\"red\">");
  strcat(maliny,"Maliny: ");
  sprintf(pomoc,"%d",malinyLiczba);
  strcat(maliny, pomoc);
  strcat(maliny,"</span></b>");

  GtkWidget *ekwipunek=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_transient_for(GTK_WINDOW(ekwipunek),GTK_WINDOW(oknoProgramu));
  gtk_container_set_border_width(GTK_CONTAINER(ekwipunek),ODSTEP_MIEDZY_PRZYCISKAMI*3);

  g_signal_connect(G_OBJECT(ekwipunek),"destroy",G_CALLBACK(gtk_widget_destroy),
                   ekwipunek);

  GtkWidget *lista=gtk_list_box_new();
  gtk_container_add(GTK_CONTAINER(ekwipunek),lista);

  GtkWidget *wiersz=gtk_list_box_row_new();

  GtkWidget *pudelko=gtk_grid_new();
  gtk_container_set_border_width(GTK_CONTAINER(pudelko),ODSTEP_MIEDZY_PRZYCISKAMI);
  gtk_grid_set_column_spacing(GTK_GRID(pudelko),ODSTEP_MIEDZY_PRZYCISKAMI*5);

  GtkWidget *napis=gtk_label_new("");
  gtk_label_set_markup(GTK_LABEL(napis),maliny);

  gtk_grid_attach(GTK_GRID(pudelko),napis,0,0,1,1);
//  gtk_list_box_row_set_header(GTK_LIST_BOX_ROW(wiersz),napis);

  GtkWidget *przycisk = gtk_button_new_with_label("Zjedz");
  g_signal_connect(G_OBJECT(przycisk),"clicked",G_CALLBACK(zjedzMaline),wiersz);
  gtk_grid_attach(GTK_GRID(pudelko),przycisk,1,0,1,1);

  gtk_container_add(GTK_CONTAINER(wiersz),pudelko);

  gtk_list_box_insert(GTK_LIST_BOX(lista),wiersz,-1);

  gtk_widget_show_all(ekwipunek);
  return;
}

void zbior()
{
  int m=KolumnyWMapie(NULL);
  Pozycja gracz = pozycjaGracza(NULL,MojID(NULL));
  int *mapa=MapaWTablicy(NULL);
  //jeśli stoimy na polu malin
  if(mapa[gracz.x*m+gracz.y]==4)
  {
    int PE=PunktyEnergii(NULL,MojID(NULL));
    PE-=KOSZT_ZBIORU_MALIN;
    PunktyEnergii(&PE, MojID(NULL));
    wyswietlStatystykiGracza();
    Gracz ja=InfoOMnie(NULL);
    ja.maliny++;
    InfoOMnie(&ja);
    return;
  }
  return;
}

static void stworzPrawaPoloweEkranuGry(GtkWidget *panel, GtkWidget *oknoProgramu)
{
  //funkcja tworząca prawą część ekranu gry
  GtkWidget *pudelko = gtk_box_new(GTK_ORIENTATION_VERTICAL,ODSTEP_MIEDZY_PRZYCISKAMI);
  gtk_container_set_border_width(GTK_CONTAINER(pudelko),ODSTEP_MIEDZY_PRZYCISKAMI);
  gtk_box_set_homogeneous(GTK_BOX(pudelko),TRUE);
  gtk_paned_add2(GTK_PANED(panel),pudelko);

  GtkWidget *powrotDoMenu=gtk_button_new_with_label("Powrót do menu");
  g_signal_connect(G_OBJECT(powrotDoMenu),"clicked",G_CALLBACK(wrocDoMenuGlownego),
                   oknoProgramu);
  gtk_box_pack_start(GTK_BOX(pudelko),powrotDoMenu,TRUE,TRUE,ODSTEP_MIEDZY_PRZYCISKAMI);

  GtkWidget *statystyki=gtk_label_new("");
  gtk_label_set_justify(GTK_LABEL(statystyki),GTK_JUSTIFY_LEFT);
  gtk_label_set_xalign(GTK_LABEL(statystyki),0.0);
  gtk_label_set_lines(GTK_LABEL(statystyki),LICZBA_LINI_STATYSTYK);
  gtk_label_set_max_width_chars(GTK_LABEL(statystyki),MAX_LICZBA_ZNAKOW_W_LINII_STATYSTYK);
  gtk_box_pack_start(GTK_BOX(pudelko),statystyki,TRUE,TRUE,ODSTEP_MIEDZY_PRZYCISKAMI);
  odnosnikDoNapisuZeStatystykami(&statystyki);
  wyswietlStatystykiGracza();

  GtkWidget *siatka=gtk_grid_new();
  gtk_grid_set_column_homogeneous(GTK_GRID(siatka),TRUE);
  gtk_grid_set_row_homogeneous(GTK_GRID(siatka),TRUE);
  gtk_grid_set_column_spacing(GTK_GRID(siatka),ODSTEP_MIEDZY_PRZYCISKAMI);
  gtk_grid_set_row_spacing(GTK_GRID(siatka),ODSTEP_MIEDZY_PRZYCISKAMI);
  gtk_box_pack_start(GTK_BOX(pudelko),siatka,TRUE,TRUE,ODSTEP_MIEDZY_PRZYCISKAMI*2);

  GtkWidget *przycisk=gtk_toggle_button_new_with_label("Atak");
  g_signal_connect(G_OBJECT(przycisk),"clicked",G_CALLBACK(kliknietoPrzyciskAtaku),
                   NULL);
  gtk_grid_attach(GTK_GRID(siatka),przycisk,0,0,1,1);
  deaktywujPrzyciskAtaku(przycisk);


  przycisk=gtk_button_new_with_label("Ekwipunek");
  g_signal_connect(G_OBJECT(przycisk),"clicked",G_CALLBACK(wyswietlEkwipunek),
                   oknoProgramu);
  gtk_grid_attach(GTK_GRID(siatka),przycisk,1,0,1,1);

  przycisk=gtk_button_new_with_label("Zbiór");
  g_signal_connect(G_OBJECT(przycisk),"clicked",G_CALLBACK(zbior),NULL);
  gtk_grid_attach(GTK_GRID(siatka),przycisk,0,1,1,1);

  przycisk=gtk_button_new_with_label("Koniec tury");
  g_signal_connect(G_OBJECT(przycisk),"clicked",G_CALLBACK(koniecTury),NULL);
  gtk_grid_attach(GTK_GRID(siatka),przycisk,1,1,1,1);

  GtkWidget *siatka2=gtk_grid_new();
  gtk_grid_set_column_homogeneous(GTK_GRID(siatka2),TRUE);
  gtk_grid_set_row_homogeneous(GTK_GRID(siatka2),TRUE);
  gtk_grid_set_column_spacing(GTK_GRID(siatka2),ODSTEP_MIEDZY_PRZYCISKAMI);
  gtk_grid_set_row_spacing(GTK_GRID(siatka2),ODSTEP_MIEDZY_PRZYCISKAMI);
  gtk_box_pack_start(GTK_BOX(pudelko),siatka2,TRUE,TRUE,ODSTEP_MIEDZY_PRZYCISKAMI*2);

  //strałka w góre
  przycisk=gtk_button_new_with_label("\u2191");
  g_signal_connect(G_OBJECT(przycisk),"clicked", G_CALLBACK(WGore),NULL);
  gtk_grid_attach(GTK_GRID(siatka2),przycisk,1,0,1,1);

  //strałka w dół
  przycisk=gtk_button_new_with_label("\u2193");
  g_signal_connect(G_OBJECT(przycisk),"clicked", G_CALLBACK(WDol),NULL);
  gtk_grid_attach(GTK_GRID(siatka2),przycisk,1,2,1,1);

  //strałka w lewo
  przycisk=gtk_button_new_with_label("\u2190");
  g_signal_connect(G_OBJECT(przycisk),"clicked", G_CALLBACK(WLewo),NULL);
  gtk_grid_attach(GTK_GRID(siatka2),przycisk,0,1,1,1);

  //strałka w prawo
  przycisk=gtk_button_new_with_label("\u2192");
  g_signal_connect(G_OBJECT(przycisk),"clicked", G_CALLBACK(WPrawo),NULL);
  gtk_grid_attach(GTK_GRID(siatka2),przycisk,2,1,1,1);

  return;
}

static int zarzadajDanychDoGenerowaniaPlanszy(int *n, int *m, int *s,char nazwa[],
                                        GtkWidget *glowneOknoProgramu)
{
  //tworzenie okna pop-up z prośbą o podanie danych
  //potrzebnych do wygenerowania mapy
  char wysokosc[LICZBA_CYFR], szerokosc[LICZBA_CYFR], LZSP[LICZBA_CYFR];
  GtkWidget *okno=gtk_dialog_new();
  gtk_window_set_modal(GTK_WINDOW(okno),TRUE);
  gtk_window_set_transient_for(GTK_WINDOW(okno),GTK_WINDOW(glowneOknoProgramu));
  gtk_container_set_border_width(GTK_CONTAINER(okno),ODSTEP_MIEDZY_PRZYCISKAMI*2);
  gtk_window_set_default_size(GTK_WINDOW(okno),1,1);

  GtkWidget *zawartosc=gtk_dialog_get_content_area(GTK_DIALOG(okno));
  gtk_box_set_spacing(GTK_BOX(zawartosc),ODSTEP_MIEDZY_PRZYCISKAMI);

  GtkWidget *napis=gtk_label_new("Podaj nazwe mapy:");
  gtk_container_add(GTK_CONTAINER(zawartosc),napis);

  GtkWidget *wprowadzanieN=gtk_entry_new();
  gtk_entry_set_max_length(GTK_ENTRY(wprowadzanieN),DLUGOSC_NAZWY_SCIEZKI-30);
  gtk_entry_set_text(GTK_ENTRY(wprowadzanieN),"test");
  gtk_container_add(GTK_CONTAINER(zawartosc),wprowadzanieN);

  napis=gtk_label_new("Podaj wysokość mapy (zalecana 10-100):");
  gtk_container_add(GTK_CONTAINER(zawartosc),napis);

  GtkWidget *wprowadzanieW=gtk_entry_new();
  gtk_entry_set_max_length(GTK_ENTRY(wprowadzanieW),LICZBA_CYFR);
  gtk_entry_set_text(GTK_ENTRY(wprowadzanieW),"20");
  gtk_container_add(GTK_CONTAINER(zawartosc),wprowadzanieW);

  napis=gtk_label_new("Podaj szerokość mapy (zalecana 10-100):");
  gtk_container_add(GTK_CONTAINER(zawartosc),napis);

  GtkWidget *wprowadzanieS=gtk_entry_new();
  gtk_entry_set_max_length(GTK_ENTRY(wprowadzanieS),LICZBA_CYFR);
  gtk_entry_set_text(GTK_ENTRY(wprowadzanieS),"20");
  gtk_container_add(GTK_CONTAINER(zawartosc),wprowadzanieS);

  napis=gtk_label_new("Podaj liczbę Zimnokrwistych Smoków Pustynnych:");
  gtk_container_add(GTK_CONTAINER(zawartosc),napis);

  GtkWidget *wprowadzanieZSP=gtk_entry_new();
  gtk_entry_set_max_length(GTK_ENTRY(wprowadzanieZSP),LICZBA_CYFR);
  gtk_entry_set_text(GTK_ENTRY(wprowadzanieZSP),"5");
  gtk_container_add(GTK_CONTAINER(zawartosc),wprowadzanieZSP);

  gtk_dialog_add_button(GTK_DIALOG(okno),"OK",GTK_RESPONSE_OK);

  gtk_widget_show_all(okno);
  if(gtk_dialog_run(GTK_DIALOG(okno))==GTK_RESPONSE_OK)
  {
    strcpy(nazwa,gtk_entry_get_text(GTK_ENTRY(wprowadzanieN)));
    strcpy(wysokosc,gtk_entry_get_text(GTK_ENTRY(wprowadzanieW)));
    strcpy(szerokosc,gtk_entry_get_text(GTK_ENTRY(wprowadzanieS)));
    strcpy(LZSP,gtk_entry_get_text(GTK_ENTRY(wprowadzanieZSP)));
    *n=atoi(wysokosc);
    *m=atoi(szerokosc);
    *s=atoi(LZSP);
    gtk_widget_destroy(okno);
    return 1;
  }
  //jeśli dane nie zostaną podane wróć do menu głównego
  gtk_widget_destroy(okno);
  return 0;
}

static void wyjdzZGryBedacWEkranieGry()
{
  //funkcja zapisująca dane i powodująca zamknięcie programu,
  //wywoływana po naciśnięciu x w ekranie gry
  int pomoc=1;
  petlaStop(&pomoc);
  zapiszMgleWojny(MojID(NULL),KolumnyWMapie(NULL),WierszeWMapie(NULL));
  int S=liczbaSmokow(NULL);
  Smok *Stab=TablicaZeSmokami(NULL);
  zapiszSmokiDoPliku(Stab,S,odnosnikDoNazwyMapy(NULL));
  zapiszParametryGracza(MojID(NULL));
  zapiszParametryGracza(TwojID(NULL));
  free(MapaWTablicy(NULL));
  free(TablicaObecnosciSmokow(NULL));
  free(TablicaZeSmokami(NULL));
  free(mglaWojny(NULL));
  odlaczGraczaOdMapy(odnosnikDoNazwyMapy(NULL),MojID(NULL));
  zapiszAktualnaTura();
  zamknijPlikiKolejkowe();
  gtk_main_quit();
  return;
}

void wyswietlSmokiNaPoczatku()
{
  //pętla wyświetlająca smoki na mapie
  Smok *Stab=TablicaZeSmokami(NULL);
  int S=liczbaSmokow(NULL);

  for(int i=0;i<S;i++)
  {
    ustawSmokaNaMapie(Stab[i].pozycja);
  }
  return;
}

static void stworzEkranGry(GtkWidget *oknoProgramu, int n, int m, char nazwa[])
{
  //funkcja tworząca Ekran gry (wywołuje funkcje tworzące lewą i prawą
  //część), a także wywołuje funkcje inicjalizujące
  GtkWidget *menuGlowne=gtk_bin_get_child(GTK_BIN(oknoProgramu));
  gtk_widget_destroy(menuGlowne);

  g_signal_handlers_disconnect_by_func(G_OBJECT(oknoProgramu),
                                       G_CALLBACK(gtk_main_quit), NULL);
  g_signal_connect(G_OBJECT(oknoProgramu),"destroy",G_CALLBACK(wyjdzZGryBedacWEkranieGry)
                   ,NULL);

  GtkWidget *panel = gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
  gtk_container_add(GTK_CONTAINER(oknoProgramu),panel);


  zaladujMgleWojny(MojID(NULL),nazwa,n,m);


  stworzLewaPoloweEkranuGry(panel,n,m,nazwa);
  stworzPrawaPoloweEkranuGry(panel,oknoProgramu);
  int pozycjaDzielnikaPaneli;
  gtk_window_get_size(GTK_WINDOW(oknoProgramu),&pozycjaDzielnikaPaneli,NULL);
  pozycjaDzielnikaPaneli=pozycjaDzielnikaPaneli-SZEROKOSC_PRAWEJ_CZESCI_EKRANU;
  gtk_paned_set_position(GTK_PANED(panel),pozycjaDzielnikaPaneli);
  gtk_widget_show_all(panel);

  //funkcje inicjalizujące
  zaladujAktualnaTure(nazwa);
  KolumnyWMapie(&m);
  WierszeWMapie(&n);
  zaladujParametryGracza(MojID(NULL),nazwa);
  zaladujParametryGracza(TwojID(NULL),nazwa);
  pobierzSmokiZPliku(nazwa,n,m);
  otworzPlikiKolejkowe(nazwa,MojID(NULL));
  pobierzZPlikowPozycjeGraczy();
  wyswietlStatystykiGracza();
  ustawGraczaNaMapie(pozycjaGracza(NULL,MojID(NULL)),MojID(NULL));
  ustawGraczaNaMapie(pozycjaGracza(NULL,TwojID(NULL)),TwojID(NULL));
  wyswietlSmokiNaPoczatku();

  int pomoc=0;
  petlaStop(&pomoc);
  g_timeout_add(ODSTEP_CZASOWY_MIEDZY_SPRAWDZENIEM_DANYCH,
                sprawdzanieNadszlychWiadomosci,NULL);

  return;
}

static void stworzNowaGre(GtkWidget *ojciec, GtkWidget *oknoProgramu)
{
  //funkcja wywoływana po naciśnieciu przycisku "nowa
  //gra" w menu głównym, wywołuje prośbę o podanie danych do
  //generowania mapy i w przypadku powodzenia pobierania danych
  //generuje mapę, przypisuje graczowi id i wywołuje tworzenie
  //ekranu gry
  int n=0, m=0, S=0;
  char nazwa[DLUGOSC_NAZWY_SCIEZKI];

  if(0==zarzadajDanychDoGenerowaniaPlanszy(&n,&m,&S,nazwa,oknoProgramu))
  {
    return;
  }
  odnosnikDoNazwyMapy(nazwa);
  generatorMapy(n,m,S,nazwa);
  char id='A';
  MojID(&id);
  id='B';
  TwojID(&id);
  if(-1==ustawPodlaczenieDoMapy(nazwa,'A',oknoProgramu))
  {
    return;
  }
  stworzEkranGry(oknoProgramu,n,m,nazwa);

  return;
}

static int zarzadajDanychDoWczytaniaMapy(char nazwa[], GtkWidget *oknoProgramu, char *id)
{
  //prośba o podanie danych potrzebnych do wczytania mapy
  //w przypadku niepowodzenia pobrania danych (gracz ich
  //nie podał) zwraca informacje o powrocie do menu głównego
  GtkWidget *oknoPopup = gtk_dialog_new();
  gtk_window_set_transient_for(GTK_WINDOW(oknoPopup),GTK_WINDOW(oknoProgramu));
  gtk_container_set_border_width(GTK_CONTAINER(oknoPopup),ODSTEP_MIEDZY_PRZYCISKAMI*2);
  gtk_window_set_default_size(GTK_WINDOW(oknoPopup),1,1);

  GtkWidget *zawartosc = gtk_dialog_get_content_area(GTK_DIALOG(oknoPopup));
  gtk_box_set_spacing(GTK_BOX(zawartosc),ODSTEP_MIEDZY_PRZYCISKAMI);

  GtkWidget *napis = gtk_label_new("Podaj nazwę gry do której chcesz dołączyć:");
  gtk_container_add(GTK_CONTAINER(zawartosc),napis);

  GtkWidget *wprowadzanie=gtk_entry_new();
  gtk_entry_set_max_length(GTK_ENTRY(wprowadzanie),DLUGOSC_NAZWY_SCIEZKI-30);
  gtk_entry_set_text(GTK_ENTRY(wprowadzanie),"test");
  gtk_container_add(GTK_CONTAINER(zawartosc),wprowadzanie);

  GtkWidget *napisT = gtk_label_new("Podaj typ gracza jako który chcesz dołączyć:\n\
('A' - Gospodarz lub 'B' - Gość)");
  gtk_container_add(GTK_CONTAINER(zawartosc),napisT);

  GtkWidget *wprowadzanieT = gtk_entry_new();
  gtk_entry_set_max_length(GTK_ENTRY(wprowadzanieT),1);
  gtk_entry_set_text(GTK_ENTRY(wprowadzanieT),"A");
  gtk_container_add(GTK_CONTAINER(zawartosc),wprowadzanieT);

  gtk_dialog_add_button(GTK_DIALOG(oknoPopup),"Ok",GTK_RESPONSE_OK);

  gtk_widget_show_all(oknoPopup);
  if(GTK_RESPONSE_OK==gtk_dialog_run(GTK_DIALOG(oknoPopup)))
  {
    strcpy(nazwa,gtk_entry_get_text(GTK_ENTRY(wprowadzanie)));
    char pomocnicza[3];
    strcpy(pomocnicza,gtk_entry_get_text(GTK_ENTRY(wprowadzanieT)));
    (*id)=pomocnicza[0];
    gtk_widget_destroy(oknoPopup);
    return 1;
  }
  gtk_widget_destroy(oknoPopup);
  return 0;
}

static int policzLiczbeWierszyIKolumnWPliku(int *n, int *m, char nazwa[])
{
  //liczenie wierszy i kolumn w pliku mapy, na potrzeby
  //wczytywania mapy do tablicy
  //wywoływana jest ta funkcja podczas kontynuacji gry
  char sciezka[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(sciezka,"./");
  strcat(sciezka,nazwa);
  if(0!=access(sciezka,F_OK))
  {
    fprintf(stderr,"Błąd dostępu do folderu:%s\n",sciezka);
    return -1;
  }
  strcat(sciezka,"/mapa");
  FILE *plikMapy=fopen(sciezka,"r");
  if(plikMapy==NULL)
  {
    fprintf(stderr,"Błąd dostępu do pliku:%s\n",sciezka);
    return -2;
  }
  *n=0, *m=0;
  int znak=getc(plikMapy);
  int pomoc;
  while(znak!='\n' && znak!=EOF)
  {
    ungetc(znak,plikMapy);
    (*m)++;
    fscanf(plikMapy,"<%d>",&pomoc);
    znak=getc(plikMapy);
  }
  rewind(plikMapy);
  znak=getc(plikMapy);
  while(znak!=EOF)
  {
    if(znak=='\n')
    {
      (*n)++;
    }
    znak=getc(plikMapy);
  }
  fclose(plikMapy);
  return 0;
}

void wyswietlBlad(GtkWidget *oknoProgramu, char komunikat[])
{
  //wyśwuetlanie okienka pop-up z zadanym komunikatem
  GtkWidget *oknoPopup = gtk_dialog_new();
  gtk_window_set_transient_for(GTK_WINDOW(oknoPopup),GTK_WINDOW(oknoProgramu));
  gtk_container_set_border_width(GTK_CONTAINER(oknoPopup),ODSTEP_MIEDZY_PRZYCISKAMI);
  gtk_window_set_default_size(GTK_WINDOW(oknoPopup),1,1);

  GtkWidget *zawartosc = gtk_dialog_get_content_area(GTK_DIALOG(oknoPopup));
  gtk_widget_set_size_request(zawartosc,SZEROKOSC_OKNA_WIADOMOSCI_POPUP,
                              WYSOKOSC_OKNA_WIADOMOSCI_POPUP);
  gtk_container_set_border_width(GTK_CONTAINER(zawartosc),ODSTEP_MIEDZY_PRZYCISKAMI);

  GtkWidget *napis=gtk_label_new(komunikat);
  gtk_label_set_max_width_chars(GTK_LABEL(napis),MAXYMALNA_LICZBA_ZNAKOW_W_OKNACH_POPUP);
  gtk_label_set_line_wrap(GTK_LABEL(napis),TRUE);
  gtk_container_add(GTK_CONTAINER(zawartosc),napis);

  gtk_dialog_add_button(GTK_DIALOG(oknoPopup),"Ok",GTK_RESPONSE_OK);

  gtk_widget_show_all(oknoPopup);
  gtk_dialog_run(GTK_DIALOG(oknoPopup));
  gtk_widget_destroy(oknoPopup);
  return;
}

static void wczytajGre(GtkWidget *ojciec, GtkWidget *oknoProgramu)
{
  //funkcja wywoływana po naciśnięciu w "Menu głównym" przycisku
  //"Kontynuuj grę"
  int n,m;
  char id;
  char nazwa[DLUGOSC_NAZWY_SCIEZKI];
  if(0==zarzadajDanychDoWczytaniaMapy(nazwa,oknoProgramu,&id))
  {
    return;
  }
  odnosnikDoNazwyMapy(nazwa);
  int pomoc=policzLiczbeWierszyIKolumnWPliku(&n,&m,nazwa);
  switch (pomoc)
  {
    case -1:
      wyswietlBlad(oknoProgramu,"Błąd dostępu do podanej gry.\n\
Proszę sprawdzić czy podana nazwa nie zawierała żadnych literówek.");
    return;
    break;

    case -2:
      wyswietlBlad(oknoProgramu,"Błąd dostępu do pliku mapy. Proszę spróbować jeszcze raz.");
    return;
    break;

    case 0:
      MojID(&id);
      if(id=='A')
      {
        char p='B';
        TwojID(&p);
      }
      else
      {
        char p='A';
        TwojID(&p);
      }
      if(-1==ustawPodlaczenieDoMapy(nazwa,id,oknoProgramu))
      {
        break;
      }
      stworzEkranGry(oknoProgramu,n,m,nazwa);
    break;

    default:
      fprintf(stderr,"Funkacja policzLiczbeWierszyIKolumn zwróciła nieznaną wartość\n");
    return;
  }
  return;
}

GtkWidget* stworzOknoGlowne()
{
  //funkcja tworząca główne okno programu
  GtkWidget *oknoProgramu=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(oknoProgramu),"Die Kaltbluetigen Wuestendrachen");
  gtk_window_set_position(GTK_WINDOW(oknoProgramu),GTK_WIN_POS_CENTER);
  gtk_window_set_default_size(GTK_WINDOW(oknoProgramu),PODSTAWOWA_SZEROKOSC_OKNA,
                              PODSTAWOWA_WYSOKOSC_OKNA);
  g_signal_connect(G_OBJECT(oknoProgramu),"destroy",G_CALLBACK(gtk_main_quit),NULL);

  odnosnikDoOknaProgramu(oknoProgramu);

  //ustaw domyślne statystyki
  UstawDomyslneStatystykiGracza();

  stworzMenuGlowne(oknoProgramu);

  return oknoProgramu;
}

static void stworzMenuGlowne(GtkWidget *oknoProgramu)
{
  //tworzenie menu głównego
  GtkWidget *pudelkoZPrzyciskami=gtk_box_new(GTK_ORIENTATION_VERTICAL,
                                 ODSTEP_MIEDZY_PRZYCISKAMI);
  gtk_box_set_homogeneous(GTK_BOX(pudelkoZPrzyciskami),TRUE);
  int marginesPrzycisku;
  gtk_window_get_size(GTK_WINDOW(oknoProgramu),&marginesPrzycisku,NULL);
  marginesPrzycisku-=SZEROKOC_PRZYCISKOW_W_MENU_GLOWNYM;
  marginesPrzycisku/=2;
  gtk_widget_set_margin_top(pudelkoZPrzyciskami,SZEROKOSC_MARGINESOW);
  gtk_widget_set_margin_bottom(pudelkoZPrzyciskami,SZEROKOSC_MARGINESOW);
  gtk_widget_set_margin_start(pudelkoZPrzyciskami,marginesPrzycisku);
  gtk_widget_set_margin_end(pudelkoZPrzyciskami,marginesPrzycisku);
  gtk_container_add(GTK_CONTAINER(oknoProgramu),pudelkoZPrzyciskami);

  // dodaj przycisk rozpoczęcia nowej gry

  GtkWidget *przycisk=gtk_button_new_with_label("Nowa Gra");
  g_signal_connect(G_OBJECT(przycisk),"clicked",G_CALLBACK(stworzNowaGre),
                   (gpointer)oknoProgramu);
  gtk_box_pack_start(GTK_BOX(pudelkoZPrzyciskami),przycisk,TRUE,TRUE,0);

  //dodaj przycisk kontynuacji/dołączenia do gry
  przycisk=gtk_button_new_with_label("Kontynuuj Grę");
  g_signal_connect(G_OBJECT(przycisk),"clicked",G_CALLBACK(wczytajGre),oknoProgramu);
  gtk_box_pack_start(GTK_BOX(pudelkoZPrzyciskami),przycisk,TRUE,TRUE,0);

  przycisk=gtk_button_new_with_label("Otwórz okno Debug");
  g_signal_connect(G_OBJECT(przycisk),"clicked",G_CALLBACK(oknoTrybuDebug),NULL);
  gtk_box_pack_start(GTK_BOX(pudelkoZPrzyciskami),przycisk,TRUE,TRUE,0);

  przycisk=gtk_button_new_with_label("Wyjście");
  g_signal_connect(G_OBJECT(przycisk),"clicked",G_CALLBACK(gtk_main_quit),NULL);
  gtk_box_pack_start(GTK_BOX(pudelkoZPrzyciskami),przycisk,TRUE,TRUE,0);

  gtk_widget_show_all(oknoProgramu);
  return;
}

void sprawdzCzyKtosStoiNaDanymPolu(Pozycja pole, char id)
{
  //sprawdzanie czy ktoś przebywa na danym polu
  //dla celów funkcji odkrywającej mgłę wojny
  //po wejściu na dane pole
  int *SOtab=TablicaObecnosciSmokow(NULL);
  int m=KolumnyWMapie(NULL);
  if(SOtab[pole.x*m+pole.y]==1)
  {
    ustawSmokaNaMapie(pole);
    return;
  }
  Pozycja graczA=pozycjaGracza(NULL,'A');
  if(id!='A' && graczA.x==pole.x && graczA.y==pole.y)
  {
    ustawGraczaNaMapie(graczA,'A');
    return;
  }
  Pozycja graczB=pozycjaGracza(NULL,'B');
  if(id!='B' && graczB.x==pole.x && graczB.y==pole.y)
  {
    ustawGraczaNaMapie(graczB,'B');
    return;
  }
  return;
}

void odkryjMgleDookolaPola(Pozycja pole, char id)
{
  //funkcja odkrywa wszystkie pola dookoła zadanego,
  //usuwając z nich mgłę wojny, po odkryciu sprawdza,
  //czy ktoś stał na odkrytym polu i jeśli tak, to
  //wyświetla go na tym polu
  int *mgla=mglaWojny(NULL);
  int m=KolumnyWMapie(NULL);
  int p1[]={1,1,1,0,-1,-1,-1,0};
  int p2[]={1,0,-1,1,1,-1,0,-1};
  for(int i=0;i<8;i++)
  {
    mgla[(pole.x+p1[i])*m+pole.y+p2[i]]=1;
    Pozycja nowa;
    nowa.x=pole.x+p1[i];
    nowa.y=pole.y+p2[i];
    przywrocPoleTerenu(nowa,m);
    sprawdzCzyKtosStoiNaDanymPolu(nowa,id);
  }
  return;
}

void ustawGraczaNaMapie(Pozycja gracz, char id)
{
  //funkcja wyświetlająca gracza o zadanym id na polu w mapie
  int x=gracz.x;
  int y=gracz.y;
  int *mgla=mglaWojny(NULL);
  int m=KolumnyWMapie(NULL);
  if(MojID(NULL)==id)
  {
    mgla[x*m+y]=1;
    odkryjMgleDookolaPola(gracz,id);
  }
//  fprintf(stderr,"Pozycja gracza %c:%d %d\n",id,x,y);
  //wyciągnij panel dwuczęściowy z okna ekranu
  GtkWidget *panel=gtk_bin_get_child(GTK_BIN(odnosnikDoOknaProgramu(NULL)));
  //wyciągnij yciągnij okno przewijane z części z mapą
  GtkWidget *oknoPrzewijane=gtk_paned_get_child1(GTK_PANED(panel));
  //przejdź do adaptera w oknie przewijanym, który jest w postaci viewport
  GtkWidget *adaptator=gtk_bin_get_child(GTK_BIN(oknoPrzewijane));
  //wyciagnij okno które jest a adapterze
  GtkWidget *siatka=gtk_bin_get_child(GTK_BIN(adaptator));
  GtkWidget *obrazek=gtk_grid_get_child_at(GTK_GRID(siatka),y,x);

  char gracz1[DLUGOSC_NAZWY_SCIEZKI];
  char gracz2[DLUGOSC_NAZWY_SCIEZKI];
  char sciezkaMgla[DLUGOSC_NAZWY_SCIEZKI];

  stworzSciezkeDoObrazkuMgly(sciezkaMgla);
  stworzSciezkiDoObrazkowGraczy(gracz1,gracz2);

  if(mgla[x*m+y]==1)
  {
    if(id=='A')
      gtk_image_set_from_file(GTK_IMAGE(obrazek),gracz1);
    else
      gtk_image_set_from_file(GTK_IMAGE(obrazek),gracz2);

  }
  else
  {
    gtk_image_set_from_file(GTK_IMAGE(obrazek),sciezkaMgla);
  }
  return;
}

void przywrocPoleTerenu(Pozycja pole, int m)
{
  //przywracanie pola terenu do stanu jaki zostal wygenerowany
  //przez generator (zastąpienie obrazka gracza/smoka, obrazkiem
  //wygenerowanego pola)
  int x=pole.x;
  int y=pole.y;
  char sciezkaMgla[DLUGOSC_NAZWY_SCIEZKI];
  stworzSciezkeDoObrazkuMgly(sciezkaMgla);
  //x=1,y=1;
//wyciągnij panel dwuczęściowy z okna ekranu
  GtkWidget *panel=gtk_bin_get_child(GTK_BIN(odnosnikDoOknaProgramu(NULL)));
  //wyciągnij yciągnij okno przewijane z części z mapą
  GtkWidget *oknoPrzewijane=gtk_paned_get_child1(GTK_PANED(panel));
  //przejdź do adaptera w oknie przewijanym, który jest w postaci viewport
  GtkWidget *adaptator=gtk_bin_get_child(GTK_BIN(oknoPrzewijane));
  //wyciagnij okno które jest a adapterze
  GtkWidget *siatka=gtk_bin_get_child(GTK_BIN(adaptator));
  GtkWidget *obrazek=gtk_grid_get_child_at(GTK_GRID(siatka),y,x);

  char sciezki[LICZBA_SYMBOLI][DLUGOSC_NAZWY_SCIEZKI];
  stworzTabliceSciezekDoObrazowPol(sciezki);

  int *mapa=MapaWTablicy(NULL);
  int *mgla=mglaWojny(NULL);
  int idPola=mapa[x*m+y];
  if(mgla[x*m+y]==1)
  {
    gtk_image_set_from_file(GTK_IMAGE(obrazek),sciezki[idPola]);
  }
  else
  {
    gtk_image_set_from_file(GTK_IMAGE(obrazek),sciezkaMgla);
  }

  return;
}

static void oknoUstawPunktyZycia(GtkWidget *ojciec, GtkWidget *oknoDebug)
{
  //okno pop-up dla celów okna Debug, służy ustawianiu
  //punktów PZ, danego gracza
  GtkWidget *popup=gtk_dialog_new();
  gtk_window_set_transient_for(GTK_WINDOW(popup),GTK_WINDOW(oknoDebug));
  gtk_container_set_border_width(GTK_CONTAINER(popup),
                                 ODSTEP_MIEDZY_PRZYCISKAMI*2);

  GtkWidget *zawartosc=gtk_dialog_get_content_area(GTK_DIALOG(popup));

  GtkWidget *wprowadzanie=gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(wprowadzanie),"50");
  gtk_container_add(GTK_CONTAINER(zawartosc),wprowadzanie);

  gtk_dialog_add_button(GTK_DIALOG(popup),"Ok",GTK_RESPONSE_OK);

  gtk_widget_show_all(popup);
  if(gtk_dialog_run(GTK_DIALOG(popup))==GTK_RESPONSE_OK)
  {
    char pomoc[DLUGOSC_NAZWY_SCIEZKI];
    strcpy(pomoc,gtk_entry_get_text(GTK_ENTRY(wprowadzanie)));
    int PZ=atoi(pomoc);
    PunktZycia(&PZ,MojID(NULL));
    wyswietlStatystykiGracza();
    gtk_widget_destroy(popup);
    return;
  }
  gtk_widget_destroy(popup);
  return;
}

static void oknoUstawMaliny(GtkWidget *ojciec, GtkWidget *oknoDebug)
{
  //okno pop-up dla celów okna Debug, służy ustawianiu
  //punktów PZ, danego gracza
  GtkWidget *popup=gtk_dialog_new();
  gtk_window_set_transient_for(GTK_WINDOW(popup),GTK_WINDOW(oknoDebug));
  gtk_container_set_border_width(GTK_CONTAINER(popup),
                                 ODSTEP_MIEDZY_PRZYCISKAMI*2);

  GtkWidget *zawartosc=gtk_dialog_get_content_area(GTK_DIALOG(popup));

  GtkWidget *wprowadzanie=gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(wprowadzanie),"5");
  gtk_container_add(GTK_CONTAINER(zawartosc),wprowadzanie);

  gtk_dialog_add_button(GTK_DIALOG(popup),"Ok",GTK_RESPONSE_OK);

  gtk_widget_show_all(popup);
  if(gtk_dialog_run(GTK_DIALOG(popup))==GTK_RESPONSE_OK)
  {
    char pomoc[DLUGOSC_NAZWY_SCIEZKI];
    strcpy(pomoc,gtk_entry_get_text(GTK_ENTRY(wprowadzanie)));
    int maliny=atoi(pomoc);
    Gracz ja=InfoOMnie(NULL);
    ja.maliny=maliny;
    InfoOMnie(&ja);
    gtk_widget_destroy(popup);
    return;
  }
  gtk_widget_destroy(popup);
  return;
}

static void ustawTureA()
{
  char pomoc='A';
  aktualnaTura(&pomoc);
  return;
}

static void ustawTureB()
{
  char pomoc='B';
  aktualnaTura(&pomoc);
  return;
}

static void oknoTrybuDebug()
{
  //tworzy okno Debug
  GtkWidget *oknoDebug=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_transient_for(GTK_WINDOW(oknoDebug),
                               GTK_WINDOW(odnosnikDoOknaProgramu(NULL)));
  gtk_container_set_border_width(GTK_CONTAINER(oknoDebug),ODSTEP_MIEDZY_PRZYCISKAMI*2);

  GtkWidget *pudelko=gtk_box_new(GTK_ORIENTATION_VERTICAL,ODSTEP_MIEDZY_PRZYCISKAMI);
  gtk_container_add(GTK_CONTAINER(oknoDebug),pudelko);

  GtkWidget *przycisk=gtk_button_new_with_label("Resetuj PE");
  g_signal_connect(G_OBJECT(przycisk),"clicked",G_CALLBACK(resetujPunktyEnergii),NULL);
  gtk_box_pack_start(GTK_BOX(pudelko),przycisk,TRUE,TRUE,0);

  przycisk=gtk_button_new_with_label("Resetuj PZ");
  g_signal_connect(G_OBJECT(przycisk),"clicked",G_CALLBACK(resetujPunktyZycia),NULL);
  gtk_box_pack_start(GTK_BOX(pudelko),przycisk,TRUE,TRUE,0);

  przycisk=gtk_button_new_with_label("Ustaw PZ");
  g_signal_connect(G_OBJECT(przycisk),"clicked",G_CALLBACK(oknoUstawPunktyZycia),oknoDebug);
  gtk_box_pack_start(GTK_BOX(pudelko),przycisk,TRUE,TRUE,0);

  przycisk=gtk_button_new_with_label("Tura A");
  g_signal_connect(G_OBJECT(przycisk),"clicked",G_CALLBACK(ustawTureA),NULL);
  gtk_box_pack_start(GTK_BOX(pudelko),przycisk,TRUE,TRUE,0);

  przycisk=gtk_button_new_with_label("Tura B");
  g_signal_connect(G_OBJECT(przycisk),"clicked",G_CALLBACK(ustawTureB),NULL);
  gtk_box_pack_start(GTK_BOX(pudelko),przycisk,TRUE,TRUE,0);

  przycisk=gtk_button_new_with_label("Ustaw Maliny");
  g_signal_connect(G_OBJECT(przycisk),"clicked",G_CALLBACK(oknoUstawMaliny),oknoDebug);
  gtk_box_pack_start(GTK_BOX(pudelko),przycisk,TRUE,TRUE,0);

  gtk_widget_show_all(oknoDebug);
  return;
}

void wyswietlStatystykiGracza()
{
  //funkcja wyświetlająca informacje o grze w ekranie gry
  Gracz ja=InfoOMnie(NULL);
  char napis[DLUGOSC_WIADOMOSCI];
  char pomoc[DLUGOSC_WIADOMOSCI];
  strcpy(napis,"");
  strcat(napis,"<b><span color=\"green\">");
  strcat(napis,"Tura gracza: ");
  sprintf(pomoc,"%c",aktualnaTura(NULL));
  strcat(napis,pomoc);
  strcat(napis,"</span></b>\n");
  strcat(napis,"<b><span color=\"red\">");
  strcat(napis,"Punkty Życia: ");
  sprintf(pomoc,"%d",PunktZycia(NULL,MojID(NULL)));
  strcat(napis,pomoc);
  strcat(napis,"/");
  sprintf(pomoc,"%d",ja.maxPZ);
  strcat(napis,pomoc);
  strcat(napis,"</span></b>\n<b><span color=\"#AAAA00\">");
  strcat(napis,"Punkty Energii: ");
  sprintf(pomoc,"%d",PunktyEnergii(NULL,MojID(NULL)));
  strcat(napis,pomoc);
  strcat(napis,"/");
  sprintf(pomoc,"%d",ja.maxPE);
  strcat(napis,pomoc);
  strcat(napis,"</span></b>\n");

  GtkWidget *statystyki=odnosnikDoNapisuZeStatystykami(NULL);
  gtk_label_set_markup(GTK_LABEL(statystyki),napis);

  return;
}

void wyswietlStatystykiGraczaWrazZObrazeniami(int obrazenia)
{
  //funkcja wyświetlająca informacje o grze wraz z zadanymi
  //w tej turze obrażeniami
  wyswietlStatystykiGracza();

  GtkWidget *statystyki=odnosnikDoNapisuZeStatystykami(NULL);

  char pomoc[DLUGOSC_WIADOMOSCI];
  char napis[DLUGOSC_WIADOMOSCI];
  strcpy(napis,gtk_label_get_label(GTK_LABEL(statystyki)));
  strcat(napis,"<b><span color=\"black\">");
  strcat(napis,"Zadane obrażenia: ");
  sprintf(pomoc,"%d",obrazenia);
  strcat(napis,pomoc);
  strcat(napis,"</span></b>\n");

  gtk_label_set_markup(GTK_LABEL(statystyki),napis);;

  return;
}

void ustawSmokaNaMapie(Pozycja smok)
{
  //funkcja wyświetlająca smoka, na zadanej pozycji
  char sciezka[DLUGOSC_NAZWY_SCIEZKI];
  char sciezkaMgla[DLUGOSC_NAZWY_SCIEZKI];

  stworzSciezkeDoObrazkuMgly(sciezkaMgla);
  stworzSciezkeDoObazkuSmoka(sciezka);

  GtkWidget *oknoProgramu=odnosnikDoOknaProgramu(NULL);
  GtkWidget *panel=gtk_bin_get_child(GTK_BIN(oknoProgramu));
  GtkWidget *oknoPrzewijane=gtk_paned_get_child1(GTK_PANED(panel));
  GtkWidget *adaptator=gtk_bin_get_child(GTK_BIN(oknoPrzewijane));
  GtkWidget *siatka=gtk_bin_get_child(GTK_BIN(adaptator));
  GtkWidget *obrazek=gtk_grid_get_child_at(GTK_GRID(siatka),
                                           smok.y,smok.x);

  int *mgla=mglaWojny(NULL);
  int m=KolumnyWMapie(NULL);
  if(mgla[smok.x*m+smok.y]==1)
  {
    gtk_image_set_from_file(GTK_IMAGE(obrazek),sciezka);
  }
  else
  {
    gtk_image_set_from_file(GTK_IMAGE(obrazek),sciezkaMgla);
  }

  return;
}

void pobierzSmokiZPliku(char nazwa[], int n, int m)
{
  //wczytywanie smoków z pliku do tablicy
  char sciezka[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(sciezka,"./");
  strcat(sciezka,nazwa);
  strcat(sciezka,"/smoki");

  FILE *plikSmoki = fopen(sciezka,"r");
  if(plikSmoki==NULL)
  {
    wyswietlBlad(odnosnikDoOknaProgramu(NULL),"Błąd otwarcia pliku ze smokami.");
    fprintf(stderr,"Błąd otwarcia pliku ze smokami w funkcji pobierzSmokiZPliku.");
    fprintf(stderr,"Scieżka:%s\n", sciezka);
    return;
  }

  int S;
  Smok sm;
  Smok *Stab;
  int *SOtab;

  fscanf(plikSmoki,"Liczba smoków:%d\n",&S);
  Stab=przydzielPamiecNaTabliceSmokow(S);
  SOtab=przydzielPamiecNaTabliceObecnosciSmokow(n,m);
  TablicaObecnosciSmokow(SOtab);
  TablicaZeSmokami(Stab);

  for(int i=0;i<S;i++)
  {
    fscanf(plikSmoki,"Pozycja:%d %d PE:%d maxPE:%d PZ:%d\n", &sm.pozycja.x,
            &sm.pozycja.y,&sm.PE,&sm.maxPE,&sm.PZ);
    SOtab[sm.pozycja.x*m+sm.pozycja.y]=1;
    Stab[i]=sm;
  }
  liczbaSmokow(&S);

  fclose(plikSmoki);
  return;
}

void wyswietlOkienkoWygranej(char wiadomosc[DLUGOSC_WIADOMOSCI])
{
  //okno pop-up wyświetlające wiadomość o wygranej
  //gracza, lub jego przegranej
  GtkWidget *oknoProgramu=odnosnikDoOknaProgramu(NULL);

  GtkWidget *oknoWygranej=gtk_dialog_new();
  gtk_window_set_transient_for(GTK_WINDOW(oknoWygranej),GTK_WINDOW(oknoProgramu));
  GtkWidget *zawartosc=gtk_dialog_get_content_area(GTK_DIALOG(oknoWygranej));
  gtk_container_set_border_width(GTK_CONTAINER(zawartosc),ODSTEP_MIEDZY_PRZYCISKAMI*5);
  gtk_widget_set_size_request(zawartosc,SZEROKOSC_OKNA_WIADOMOSCI_POPUP,
                              WYSOKOSC_OKNA_WIADOMOSCI_POPUP);

  GtkWidget *napis=gtk_label_new(wiadomosc);
  gtk_container_add(GTK_CONTAINER(zawartosc),napis);

  gtk_dialog_add_button(GTK_DIALOG(oknoWygranej),"Ok",GTK_RESPONSE_OK);
  gtk_widget_show_all(oknoWygranej);
  gtk_dialog_run(GTK_DIALOG(oknoWygranej));

  gtk_widget_destroy(oknoWygranej);

  return;
}

