#include "generator_naglowek.h"

int ustawGraczyNaPozycjachPoczatkowych(int n, int m, int tab[n][m], Pozycja *gracz1,
                                        Pozycja *gracz2, Pozycja wieza);
void rozmiescSmoki(int n, int m, int S, Pozycja gracz1, Pozycja gracz2, int tab[n][m],
                   char nazwa[]);

static FILE* otworzPlikMapy(char nazwa[])
{
  //tworzy plik przechowujący mapę i go otwiera
  char sciezka[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(sciezka,"./");
  strcat(sciezka,nazwa);
  //jeśli nie istnieje folder ./nazwa to go utwórz
  if(access(sciezka,F_OK)!=0)
  {
    mkdir(sciezka, 0777);
  }
  strcat(sciezka,"/mapa");
  //otwórz plik do zapisu
  FILE *mapa=fopen(sciezka,"w");
  //sprawdz czy nie wystąpił błąd
  if(mapa==NULL)
  {
    fprintf(stderr,"Błąd dostępu do \"%s\" w celu zapisania mapy.\n",sciezka);
    return NULL;
  }
  return mapa;
}

void stworzTabliceSymboli(char symbole[])
{
  //jakie id odpowiadają konkretnym polom
  symbole[0]='X'; //pole blokady
  symbole[1]='P'; //pole łąki (pola)
  symbole[2]='L'; //pole lasu
  symbole[3]='G'; //pole gór
  symbole[4]='M'; //pole malin
  symbole[5]='W'; //pole wieży
  symbole[6]='B'; //pole barykady
  return;
}

void stworzPodstawowaTabliceRozkladuSymboli(int szansa[])
{
  //podstawowe szanse na powstanie pól danego typu
  //ta szansa jest modyfikowana następnie poprzez pomnożenie
  //przez 2 gdy pole górne lub lewe są tego samego typu

  //suma szans powinna być możliwie mała, w szczególności
  //pomnożona przez 4 powinna mieścić się w int

  szansa[0]=20;//szansa na blokade
  szansa[1]=25; //szansa na lake
  szansa[2]=25;//szansa na las
  szansa[3]=20; //szansa na gory
  szansa[4]=1; //szanda na maliny
  szansa[5]=0; //szansa na wieże (wieża jest wstawiana osobno)
  szansa[6]=0;
  return;
}

static char zwrocLosowySymbol(int szansa[LICZBA_SYMBOLI], int sumaSzans)
{
  //zwraca losowy symbol z możliwych do wygenerowania symboli
  //uwzględniajać zmianę prawdopodobieństwa na skutek sąsiedztwa
  //z innym polem tego samego typu
  int x=rand();
  x=x%sumaSzans;
  int suma=0;
  for(int i=0;i<LICZBA_SYMBOLI;i++)
  {
    if(suma<=x && x<(suma+szansa[i]))
    {
     // printf("Losowanie - suma:%d x:%d i:%d\n",suma,x,i);
      return i;
    }
    suma+=szansa[i];
  }
  fprintf(stderr,"Błąd w losowaniu symbolu.\n");
  return -1;
}

static int ustawTabliceRozkladuSymboliDlaDanegoPola(int n, int m, int szansa[],
                                                    int tab[n][m], int a, int b)
{
  //powoduje uwzględninie sąsiedztwa w tablicy prawdobodobieństw
  int suma=0;
  //podwajamy szanse że pojawi się takie samo pole jak to powyżej
  szansa[tab[a-1][b]]*=2;
  //podwajamy szanse że pojawi się takie samo pole jak to po lewej
  szansa[tab[a][b-1]]*=2;
  for(int i=0;i<LICZBA_SYMBOLI;i++)
  {
    suma+=szansa[i];
  }
  return suma;
}

static void wygenerujMapeWPostaciTablicy(int n, int m, int tab[n][m])
{
  //generuję mapę o n wierszach i m kolumnach do tablicy
  //w tablicy zapisywane są id danych pól

 // char symbole[LICZBA_SYMBOLI];
  int szansa[LICZBA_SYMBOLI];
 // stworzTabliceSymboli(symbole);
  for(int i=0;i<n;i++)
  {
    for(int j=0;j<m;j++)
    {
      //jeśli pole jest na jednej z krawędzi mapy
      if(i==0 || i==n-1 || j==0 || j==m-1)
      {
        //wstaw symbol blokady
        tab[i][j]=0;
        continue;
      }
      stworzPodstawowaTabliceRozkladuSymboli(szansa);
      int suma=ustawTabliceRozkladuSymboliDlaDanegoPola(n,m,szansa,tab,i,j);
      do
      {
        tab[i][j]=zwrocLosowySymbol(szansa,suma);
      }while(tab[i][j]==-1);
    }
  }
  return;
}

static void zapiszMapeDoPliku(int n, int m, int tab[n][m], FILE *plik)
{
  //funkcja zapisująca wygenerowaną mapę do otwartego pliku
  for(int i=0;i<n;i++)
  {
    for(int j=0;j<m;j++)
    {
      fprintf(plik,"<%d>",tab[i][j]);
    }
    putc('\n',plik);
  }
  return;
}

void wypiszTablice(int n, int m, int tab[n][m])
{
  //wypisanie tablicy do terminala w kolorze, do celów debugowania
  for(int i=0;i<n;i++)
  {
    for(int j=0;j<m;j++)
    {
      if(tab[i][j]==1)
      {
        printf("\033[0;32m");
      }
      if(tab[i][j]==2)
      {
        printf("\033[1;32m");
      }
      if(tab[i][j]==3)
      {
        printf("\033[1;34m");
      }
      if(tab[i][j]==4)
      {
        printf("\033[1;31m");
      }
      if(tab[i][j]==5)
      {
        printf("\033[1;33m");
      }
      printf("<%d>",tab[i][j]);
      printf("\033[0m");
    }
    putchar('\n');
  }
  return;
}

static void ustawWieze(int n, int m, int tab[n][m], Pozycja *wieza)
{
  //funkcja losowo ustwiająca wieże na mapie
  int i=rand()%(n-2);
  i++;
  int j=rand()%(m-2);
  j++;
  tab[i][j]=5;
  wieza->x=i;
  wieza->y=j;
  return;
}

void stworzKontrolerMapy(int n, int m, char nazwa[])
{
  //funkcja tworząca plik kontrolera mapy i zapisująca w nim wartości domyślne
  char sciezka[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(sciezka,"./");
  strcat(sciezka,nazwa);
  strcat(sciezka,"/kontrolerMapy");

  FILE *kontrolerMapy=fopen(sciezka,"w");
  if(kontrolerMapy==NULL)
  {
    fprintf(stderr,"Błąd dostępu do pliku kontrolera mapy: %s",sciezka);
  }

  fprintf(kontrolerMapy,"Gracze podłączeni:xx\n");
  fprintf(kontrolerMapy,"Aktualna tura:A\n");

  fclose(kontrolerMapy);
  return;
}

void stworzPlikiOGraczach(char nazwa[], Pozycja graczA, Pozycja graczB, int n, int m)
{
  //tworzenie plików graczA i graczB z wartościami domyślnymi
  char sciezka[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(sciezka,"./");
  strcat(sciezka,nazwa);
  char sciezka2[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(sciezka2,sciezka);
  strcat(sciezka,"/graczA");
  strcat(sciezka2,"/graczB");

  FILE *plikGraczA=fopen(sciezka,"w");
  FILE *plikGraczB=fopen(sciezka2,"w");

  fprintf(plikGraczA,"Pozycja:%d %d\n",graczA.x, graczA.y);
  fprintf(plikGraczA,"PŻ:%d\n",INF);
  fprintf(plikGraczA,"PE:%d\n",INF);

  fprintf(plikGraczB,"Pozycja:%d %d\n",graczB.x, graczB.y);
  fprintf(plikGraczB,"PŻ:%d\n",INF);
  fprintf(plikGraczB,"PE:%d\n",INF);

  fprintf(plikGraczA,"Maliny:0\n");
  fprintf(plikGraczB,"Maliny:0\n");

  fprintf(plikGraczA,"Mgla:");
  fprintf(plikGraczB,"Mgla:");
  for(int i=0;i<n*m;i++)
  {
    fprintf(plikGraczA,"<0>");
    fprintf(plikGraczB,"<0>");
  }
  fprintf(plikGraczA,"\n");
  fprintf(plikGraczB,"\n");

  fclose(plikGraczA);
  fclose(plikGraczB);
  return;
}

void generatorMapy(int n, int m, int S, char nazwa[])
{
  //podstawowa funkcja, generuje mapę
  int pomoc=0;
  int licznik=0;
  int nasiono=time(NULL);
  srand(nasiono);
  //próbujemy wygenerować LICZBA_PROB_LOSOWANIA mapę, jeśli się
  //nie uda stwierdzamy że podano błędne dane
  while(pomoc==0 && licznik<LICZBA_PROB_LOSOWANIA)
  {
    int tabMapa[n][m];
    //w tabMapa przechowywane są id pól
    FILE *mapaPlik=otworzPlikMapy(nazwa);
    wygenerujMapeWPostaciTablicy(n,m,tabMapa);
    Pozycja wieza;
    ustawWieze(n,m,tabMapa,&wieza);
    wypiszTablice(n,m,tabMapa);
    zapiszMapeDoPliku(n,m,tabMapa,mapaPlik);
    fclose(mapaPlik);

    //ustaw graczy
    Pozycja gracz1;
    Pozycja gracz2;
    pomoc=ustawGraczyNaPozycjachPoczatkowych(n,m,tabMapa,&gracz1,&gracz2,wieza);
    rozmiescSmoki(n,m,S,gracz1,gracz2,tabMapa,nazwa);
    //stworz pliki kontrolera mapy
    stworzPlikiOGraczach(nazwa,gracz1,gracz2,n,m);
    stworzKontrolerMapy(n,m,nazwa);
  }
  if(pomoc==0)
  {
    fprintf(stderr,"Błąd generowania mapy.\n");
    wyswietlBlad(odnosnikDoOknaProgramu(NULL),
                 "Błąd generowania mapy. Proszą sprawdzić poprawność danych");
    return;
  }
  return;
}

void obliczOdleglosciBFS(int n, int m, int odleglosci[n][m], int tab[n][m], Pozycja wieza)
{
  //obliczanie dla każdego pola niebędącego blokadą jego odległości
  //od wieży
  int p1[]={1,-1,0,0};
  int p2[]={0,0,1,-1};
  Kolejka q = stworzKolejke(4);
  odleglosci[wieza.x][wieza.y]=0;
  q=dodajElementDoKolejki(q,wieza);
  while(czyKolejkaPusta(q)==0)
  {
    Pozycja analizowana;
    q=usunElement(q,&analizowana);
    for(int i=0;i<4;i++)
    {
      Pozycja nowa;
      nowa.x=analizowana.x+p1[i];
      nowa.y=analizowana.y+p2[i];
      if(tab[nowa.x][nowa.y]!=0 && odleglosci[nowa.x][nowa.y]==-1)
      {
        odleglosci[nowa.x][nowa.y]= odleglosci[analizowana.x][analizowana.y]+1;
        q=dodajElementDoKolejki(q,nowa);
      }
    }
  }
  usunKolejke(q);
  return;
}

int ustawGraczyNaPozycjachPoczatkowych(int n, int m, int tab[n][m], Pozycja *gracz1,
                                        Pozycja *gracz2, Pozycja wieza)
{
  //ustawia graczy na pozycjach początkowych (losowych) w tym celu
  //oblicza dla każdego pola jego odległość od pola wieży
  //i następnie stara się wylosować pola w takiej samej odległości
  int odleglosci[n][m];
  for(int i=0;i<n;i++)
  {
    for(int j=0;j<m;j++)
    {
      odleglosci[i][j]=-1;
    }
  }

  obliczOdleglosciBFS(n,m,odleglosci,tab,wieza);
//  wypiszTablice(n,m,odleglosci);

  int pomoc=0;
  int proby=0;
  //próbujemy ustawić 1. gracza LICZBA_PROB_LOSOWANIA*100, jeśli
  //nie uda nam się znaleść takiej kombinacji, że obaj gracze są w tej
  //samej odległości uznajemy że takiej kombinacji nie ma
  while(pomoc==0 && proby<LICZBA_PROB_LOSOWANIA*100)
  {
    proby++;
    int x1=rand()%n;
    int y1=rand()%m;
    if(odleglosci[x1][y1]<=0)
    {
      continue;
    }
    //mając poprawnie ustawionego 1. gracza szukamy
    //LICZBA_PROB_LOSOWANIA razy pozycji dla 2. gracza
    for(int licznik=0;licznik<LICZBA_PROB_LOSOWANIA;licznik++)
    {
      int x2=rand()%n;
      int y2=rand()%m;
      if(odleglosci[x1][y1]==odleglosci[x2][y2] && (x1!=x2 || y1!=y2))
      {
        gracz1->x=x1;
        gracz1->y=y1;
        gracz2->x=x2;
        gracz2->y=y2;
        pomoc=1;
      }
    }
  }
//  fprintf(stderr,"Gracz1: %d %d Odległość:%d\n",gracz1->x,gracz1->y,
//           odleglosci[gracz1->x][gracz1->y]);
//  fprintf(stderr,"Gracz2: %d %d Odległość:%d\n",gracz2->x,gracz2->y,
//            odleglosci[gracz2->x][gracz2->y]);

  if(pomoc==0)
  {
    return 0;
  }
  return 1;
}

void zapiszSmokiDoPliku(Smok Stab[], int S, char nazwa[])
{
  //funkcja wypisująca tablicę smoków do pliku
  char sciezka[DLUGOSC_NAZWY_SCIEZKI];
  strcpy(sciezka,"./");
  strcat(sciezka,nazwa);
  strcat(sciezka,"/smoki");

  FILE *plikSmoki=fopen(sciezka,"w");
  if(plikSmoki==NULL)
  {
    wyswietlBlad(odnosnikDoOknaProgramu(NULL),"Błąd dostępu do pliku ze smokami, \
w funkcji zapiszSmokiDoPliku.");
    fprintf(stderr,"Błąd dostępu do pliku ze smokami, w funkcji zapiszSmokiDoPliku.");
    fprintf(stderr," Scieżka:%s\n",sciezka);
    return;
  }

  fprintf(plikSmoki,"Liczba smoków:%d\n",S);
  for(int i=0;i<S;i++)
  {
    fprintf(plikSmoki,"Pozycja:%d %d PE:%d maxPE:%d PZ:%d\n", Stab[i].pozycja.x,
            Stab[i].pozycja.y,Stab[i].PE,Stab[i].maxPE,Stab[i].PZ);
  }

  fclose(plikSmoki);
  return;
}

void rozmiescSmoki(int n, int m, int S, Pozycja gracz1, Pozycja gracz2, int tab[n][m],
                   char nazwa[])
{
  //generowanie smoków i następne ich zapisanie do pliku
  Smok Stab[S];
  int pomoc[n][m];
  for(int i=0;i<n;i++)
  {
    for(int j=0;j<m;j++)
    {
      pomoc[i][j]=0;
    }
  }
  //każdego smoka próbujemy umieścić na mapie LICZBA_PROB_LOSOWANIA razy,
  //jeśli się nie uda, uznajemy że nie ma dla niego miejsca na mapie
  for(int i=0;i<S;i++)
  {
    int p=0;
    for(int proby=0;proby<LICZBA_PROB_LOSOWANIA;proby++)
    {
      int xp=rand()%n;
      int yp=rand()%m;
      if((xp==gracz1.x && yp==gracz1.y) || (xp==gracz2.x && yp==gracz2.y))
      {
        continue;
      }
      if(pomoc[xp][yp]==1)
      {
        continue;
      }
      //jeśli nie jest to pole blokady ani wieży
      if(tab[xp][yp]!=0 && tab[xp][yp]!=5)
      {
        Smok sm;
        sm.pozycja.x=xp; sm.pozycja.y=yp;
        sm.maxPE=(rand()%3)+3;
        sm.PE=sm.maxPE;
        sm.PZ=(rand()%30)+40;
        Stab[i]=sm;
        pomoc[xp][yp]=1;
        p=1;
        break;
      }
    }
    //jeśli nie ustawiono żadnego smoka
    if(p==0)
    {
      S--;
      i--;
    }
  }
  zapiszSmokiDoPliku(Stab,S,nazwa);
  return;
}

