#ifndef KONTROLER_NAGLOWEK_H_INCLUDED
#define KONTROLER_NAGLOWEK_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct GraczT
{
  int maxPE;
  int maxPZ;
  int maliny;
};
typedef struct GraczT Gracz;

struct pozycjaT
{
  int x;
  int y;
};
typedef struct pozycjaT Pozycja;

struct SmokT
{
  Pozycja pozycja;
  int maxPE;
  int PE;
  int PZ;
};
typedef struct SmokT Smok;

#include "okienka_naglowek.h"
#define INF 2000000000
#define UZDROW_KONIEC_TURY 0.05f
#define UZDROW_MALINA 0.15f
#define KOSZT_ZBIORU_MALIN 2

char MojID(char *znak);
char TwojID(char *znak);
int ustawPodlaczenieDoMapy(char nazwa[],char id, GtkWidget *oknoProgramu);
void odlaczGraczaOdMapy(char nazwa[], char id);
GtkWidget* odnosnikDoOknaProgramu(GtkWidget *okno);
int* przydzielPamiecNaTabliceMapy(int n, int m);
int* MapaWTablicy(int *pam);
char* odnosnikDoNazwyMapy(char nazwa[]);
void pobierzZPlikowPozycjeGraczy();
Pozycja pozycjaGracza(Pozycja *gracz, char id);
int KolumnyWMapie(int *m);
void UstawDomyslneStatystykiGracza();
void resetujPunktyEnergii();
GtkWidget* odnosnikDoNapisuZeStatystykami(GtkWidget **napis);
int PunktyEnergii(int *punkty, char id);
void stworzTabliceEnergiiDoWejsciaNaPola(int energia[LICZBA_SYMBOLI]);
void zaladujParametryGracza(char id, char nazwa[]);
void zapiszParametryGracza(char id);
int PunktZycia(int *punkty,char id);
void resetujPunktyZycia();
Gracz InfoOMnie(Gracz *ja);
int liczbaSmokow(int *smoki);
Smok* TablicaZeSmokami(Smok *wsk);
Smok* przydzielPamiecNaTabliceSmokow(int S);
int* TablicaObecnosciSmokow(int *wsk);
int* przydzielPamiecNaTabliceObecnosciSmokow(int n, int m);
gboolean sprawdzanieNadszlychWiadomosci();
int WierszeWMapie(int *wsk);
int petlaStop(int *wsk);
int stanAtaku(int *wsk);
void wygranaGracza(char idWygranego, char idPrzegranego);
void zapiszAktualnaTura();
void zaladujAktualnaTure(char nazwa[]);
char aktualnaTura(char *id);
int* przydzielPamiecNaMgleWojny(int n, int m);
int* mglaWojny(int *wsk);
void zapiszMgleWojny(char id, int n, int m);
void zaladujMgleWojny(char id, char nazwa[],int n, int m);

#endif // KONTROLER_NAGLOWEK_H_INCLUDED
