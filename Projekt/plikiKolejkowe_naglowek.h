#ifndef PLIKIKOLEJKOWE_NAGLOWEK_H_INCLUDED
#define PLIKIKOLEJKOWE_NAGLOWEK_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

struct plikiKolejkowe
{
  FILE *wyjscie;
  FILE *wejscie;
};
typedef struct plikiKolejkowe Kolejki;

#define DLUGOSC_WIADOMOSCI 1000

void otworzPlikiKolejkowe(char nazwa[], char id);
void zamknijPlikiKolejkowe();
void wczytajWiadomosc(char wiadomosc[DLUGOSC_WIADOMOSCI]);
void wypiszWiadomoscDoPlikuKolejkowego(char wiadomosc[DLUGOSC_WIADOMOSCI]);

#endif // PLIKIKOLEJKOWE_NAGLOWEK_H_INCLUDED
