#include "kolejka.h"

Kolejka powiekszKolejke(Kolejka q);
Kolejka oproznijKolejke(Kolejka q);
void wypiszKolejke(Kolejka q);
void wypiszTabliceKolejki(Kolejka q);
Kolejka przeniesElementy(Kolejka q, int ikon, int ipocz);

/*
===============================
Lista 6, Zadanie 1
Kuba Nowak
Kolejka
===============================
*/

Kolejka stworzKolejke(int rozmiarPoczatkowy)
{
   Kolejka q;
   q.tab=malloc(sizeof(Pozycja)*rozmiarPoczatkowy);
   q.rozmiar=rozmiarPoczatkowy;
   //pierwszy pełna komórka
   q.pocz=q.tab;
   //pierwsza pusta komórka
   q.kon=q.tab;
   return q;
}

Kolejka dodajElementDoKolejki(Kolejka q, Pozycja a)
{
   //kolejka jest pełna gdy
   //początek jest na początku tablicy, a koniec na końcu
   if(q.kon==q.tab+q.rozmiar-1 && q.pocz==q.tab)
   {
      q=powiekszKolejke(q);
   }
   //lub koniec i poczatek sa koło siebie
   if(q.kon+1==q.pocz)
   {
      q=powiekszKolejke(q);
   }
   //jeśli na końcu nie ma pustych komórek a na początku są to przenieś wskaźnik końca
   //na początek tablicy
   *q.kon=a;
   q.kon++;
   if(q.kon==q.tab+q.rozmiar && q.pocz>q.tab)
   {
      q.kon=q.tab;
   }

   return q;
}

Kolejka powiekszKolejke(Kolejka q)
{
   int ikon=q.kon-q.tab, ipocz=q.pocz-q.tab;
   q.tab=(Pozycja*)realloc(q.tab,sizeof(Pozycja)*2*q.rozmiar);
   q.pocz=ipocz+q.tab;
   q.kon=ikon+q.tab;
   q=przeniesElementy(q, ikon, ipocz);
   q.rozmiar*=2;
   return q;
}

Kolejka przeniesElementy(Kolejka q, int ikon, int ipocz)
{
   //elementy w kolejce są spójnie poukładane
   if(ipocz==0 && ikon==ipocz+q.rozmiar-1)
   {
      return q;
   }
   int i=0;
   //przesuń wszystkie elementy z początku tablicy na koniec
   for(;i<ikon;i++)
   {
      *(q.tab+i+q.rozmiar)=*(q.tab+i);
   }
   //popraw indeks pierwszego wolnego miejsca
   q.kon=q.tab+q.rozmiar+i;
   return q;
}

void wypiszTabliceKolejki(Kolejka q)
{
   for(int i=0;i<q.rozmiar;i++)
   {
      printf("%d %d ", (*(q.tab+i)).x, (*(q.tab+i)).y);
   }
   putchar('\n');
   return;
}

void wypiszKolejke(Kolejka q)
{
   Pozycja *wsk=q.pocz;
   for(;wsk!=q.kon;wsk++)
   {
      if(wsk==q.tab+q.rozmiar)
      {
         wsk=q.tab;
      }
      printf("%d %d ", (*wsk).x, (*wsk).y);
   }
   putchar('\n');
   return;
}

Kolejka oproznijKolejke(Kolejka q)
{
   q.kon=q.pocz;
   return q;
}

Kolejka usunElement(Kolejka q, Pozycja *d)
{
   if(q.kon==q.pocz)
   {
      return q;
   }
   *d=*q.pocz;
   q.pocz++;
   if(q.pocz==q.tab+q.rozmiar)
   {
      q.pocz=q.tab;
   }
   return q;
}

int czyKolejkaPusta(Kolejka q)
{
   if(q.pocz==q.kon)
   {
      return 1;
   }
   return 0;
}

void usunKolejke(Kolejka q)
{
  free(q.tab);
  return;
}
