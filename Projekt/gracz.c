#include "gracz_naglowek.h"

#define STALY_MNOZNIK_OBRAZEN_GRACZ 4
#define ZMIENNY_MNOZNIK_OBRAZEN_GRACZ 3

void atakGracza(char id, Pozycja kierunekAtaku);

static void sprawdzCzyGraczJestWWiezy(int x, int y, char id)
{
  //sprawdzanie czy gracz dotarl na pole wieży, jeśli tak
  //to wywołanie funkcji o wygranej gracza, która powiadamia
  //graczy o tym i kończy grę
  int *mapa=MapaWTablicy(NULL);
  int m=KolumnyWMapie(NULL);
  int pole=mapa[x*m+y];
  if(pole==5)
  {
    int pomoc=1;
    petlaStop(&pomoc);
    char twid=(id=='A'?'B':'A');
    wygranaGracza(id,twid);
    return;
  }
  return;
}

static int sprawdzCzyMozeszWejscNaDanePole(int x, int y)
{
  //funkcja sprawdzająca czy gracz może wejść na pole które podał
  //czyli w szczególności czy ma wystarczająco PE i czy
  //na danym polu nie stoi ZSP ani inny gracz
  //0 - jeśli nie można wejść 1 - w.p.p.
  int *mapa=MapaWTablicy(NULL);
  int energia[LICZBA_SYMBOLI];
  int *SOtab=TablicaObecnosciSmokow(NULL);
  int m=KolumnyWMapie(NULL);
  stworzTabliceEnergiiDoWejsciaNaPola(energia);
  Pozycja gracz=pozycjaGracza(NULL,TwojID(NULL));

  int id_pola=mapa[x*KolumnyWMapie(NULL)+y];

  if(SOtab[x*m+y]==1)
  {
    return 0;
  }
  if(gracz.x==x && gracz.y==y)
  {
    return 0;
  }
  if(id_pola==0)
  {
    return 0;
  }

  if(PunktyEnergii(NULL,MojID(NULL))<energia[id_pola])
  {
    return 0;
  }

  return 1;
}

static void odejmijPunktyEnergiiPoWejsciuNaPole(int x, int y)
{
  //odejmowanie graczowi punktów energii które zużył na
  //wejście na zadane pole
  int *mapa=MapaWTablicy(NULL);
  int energia[LICZBA_SYMBOLI];
  stworzTabliceEnergiiDoWejsciaNaPola(energia);

  int id_pola=mapa[x*KolumnyWMapie(NULL)+y];

  int PE=PunktyEnergii(NULL, MojID(NULL));
  PE-=energia[id_pola];
  PunktyEnergii(&PE,MojID(NULL));
  wyswietlStatystykiGracza();
  return;
}

void WPrawo()
{
  //funkcja wywoływana po kliknięciu strzalki w prawo w ekranie gry
  //sprawdza czy przycisk ataku jest włączony, jeśli tak to wywołuje
  //funkcję atakującą, jeśli nie to próbuje poruszyć się graczem
  if(MojID(NULL)!=aktualnaTura(NULL))
  {
    return;
  }
  if(stanAtaku(NULL)==1)
  {
    Pozycja kierunekAtaku;
    kierunekAtaku.x=0;
    kierunekAtaku.y=1;
    atakGracza(MojID(NULL),kierunekAtaku);
  }
  Pozycja gracz=pozycjaGracza(NULL,MojID(NULL));
  if(0==sprawdzCzyMozeszWejscNaDanePole(gracz.x,gracz.y+1))
  {
    return;
  }
  przywrocPoleTerenu(gracz, KolumnyWMapie(NULL));
  gracz.y++;
  odejmijPunktyEnergiiPoWejsciuNaPole(gracz.x,gracz.y);
  pozycjaGracza(&gracz,MojID(NULL));
  ustawGraczaNaMapie(gracz,MojID(NULL));
  sprawdzCzyGraczJestWWiezy(gracz.x,gracz.y,MojID(NULL));
  return;
}

void WLewo()
{
  //funkcja wywoływana po kliknięciu strzalki w lewo w ekranie gry
  //sprawdza czy przycisk ataku jest włączony, jeśli tak to wywołuje
  //funkcję atakującą, jeśli nie to próbuje poruszyć się graczem
  if(MojID(NULL)!=aktualnaTura(NULL))
  {
    return;
  }
  if(stanAtaku(NULL)==1)
  {
    Pozycja kierunekAtaku;
    kierunekAtaku.x=0;
    kierunekAtaku.y=-1;
    atakGracza(MojID(NULL),kierunekAtaku);
  }
  Pozycja gracz=pozycjaGracza(NULL,MojID(NULL));
  if(0==sprawdzCzyMozeszWejscNaDanePole(gracz.x,gracz.y-1))
  {
    return;
  }
  przywrocPoleTerenu(gracz, KolumnyWMapie(NULL));
  gracz.y--;
  odejmijPunktyEnergiiPoWejsciuNaPole(gracz.x,gracz.y);
  pozycjaGracza(&gracz,MojID(NULL));
  ustawGraczaNaMapie(gracz,MojID(NULL));
  sprawdzCzyGraczJestWWiezy(gracz.x,gracz.y,MojID(NULL));
  return;
}

void WGore()
{
  //funkcja wywoływana po kliknięciu strzalki w góre w ekranie gry
  //sprawdza czy przycisk ataku jest włączony, jeśli tak to wywołuje
  //funkcję atakującą, jeśli nie to próbuje poruszyć się graczem
  if(MojID(NULL)!=aktualnaTura(NULL))
  {
    return;
  }
  if(stanAtaku(NULL)==1)
  {
    Pozycja kierunekAtaku;
    kierunekAtaku.x=-1;
    kierunekAtaku.y=0;
    atakGracza(MojID(NULL),kierunekAtaku);
  }
  Pozycja gracz=pozycjaGracza(NULL,MojID(NULL));
  if(0==sprawdzCzyMozeszWejscNaDanePole(gracz.x-1,gracz.y))
  {
    return;
  }
  przywrocPoleTerenu(gracz, KolumnyWMapie(NULL));
  gracz.x--;
  odejmijPunktyEnergiiPoWejsciuNaPole(gracz.x,gracz.y);
  pozycjaGracza(&gracz,MojID(NULL));
  ustawGraczaNaMapie(gracz,MojID(NULL));
  sprawdzCzyGraczJestWWiezy(gracz.x,gracz.y,MojID(NULL));
  return;
}

void WDol()
{
  //funkcja wywoływana po kliknięciu strzalki w dół w ekranie gry
  //sprawdza czy przycisk ataku jest włączony, jeśli tak to wywołuje
  //funkcję atakującą, jeśli nie to próbuje poruszyć się graczem
  if(MojID(NULL)!=aktualnaTura(NULL))
  {
    return;
  }
  if(stanAtaku(NULL)==1)
  {
    Pozycja kierunekAtaku;
    kierunekAtaku.x=1;
    kierunekAtaku.y=0;
    atakGracza(MojID(NULL),kierunekAtaku);
  }
  Pozycja gracz=pozycjaGracza(NULL,MojID(NULL));
  if(0==sprawdzCzyMozeszWejscNaDanePole(gracz.x+1,gracz.y))
  {
    return;
  }
  przywrocPoleTerenu(gracz, KolumnyWMapie(NULL));
  gracz.x++;
  odejmijPunktyEnergiiPoWejsciuNaPole(gracz.x,gracz.y);
  pozycjaGracza(&gracz,MojID(NULL));
  ustawGraczaNaMapie(gracz,MojID(NULL));
  sprawdzCzyGraczJestWWiezy(gracz.x,gracz.y,MojID(NULL));
  return;
}

void uzdrowGracza(double procent, char id)
{
  //przywracanie graczowi PZ w liczbie procent*maxPZ
  //funkcja wywoływana na koniec tury z wartością UZDROW_KONIEC_TURY
  Gracz Ja=InfoOMnie(NULL);
  int PZ=PunktZycia(NULL,id);
  PZ=PZ+Ja.maxPZ*procent;
  if(PZ>Ja.maxPZ)
  {
    PZ=Ja.maxPZ;
  }
  PunktZycia(&PZ,id);
  return;
}

int sprawdzCzyGraczZginal(char id)
{
  //funkcja sprawdzająca czy gracz ma dodatnią liczbę PZ, jeśli nie
  //wywołuje funkcję wygranaGracza z informacją o tym że przegraliśmy
  if(PunktZycia(NULL,id)<=0)
  {
    int pomoc=1;
    petlaStop(&pomoc);
//    fprintf(stderr,"Gracz %c przegrał!\n", id);
    char twid=(id=='A'?'B':'A');
    wygranaGracza(twid,id);
    return 1;
  }
  return 0;
}

Smok znajdzSmokaNaDanymPolu(Pozycja smok, int *nr)
{
  //funkcja zwracająca nr Smoka w tablicy na podstawie jego pozycji
  //na mapie, funkcja na potrzeby funkcji zaatakujSmoka
  int S=liczbaSmokow(NULL);
  Smok *Stab =TablicaZeSmokami(NULL);

  for(int i=0;i<S;i++)
  {
    if(Stab[i].pozycja.x==smok.x && Stab[i].pozycja.y==smok.y)
    {
      *nr=i;
      return Stab[i];
    }
  }
  Smok sm;
  sm.maxPE=-1;
  return sm;
}

void zaatakujSmoka(char id, Smok sm, int nr, Smok *Stab,
                   int *SOtab, int S)
{
  //funkcja wywoływana przez atakGracza, jeśli na polu atakowanym
  //zostanie wykryty smok. Następuje zmniejszenie liczby PZ smoka
  //i jeśli przestała ona być dodatnia to następuje usunięcie smoka
  if(sm.maxPE==-1)
  {
    deaktywujPrzyciskAtaku(NULL);
    return;
  }
  int obrazenia=PunktyEnergii(NULL,id)*((rand()%ZMIENNY_MNOZNIK_OBRAZEN_GRACZ)+
                                        STALY_MNOZNIK_OBRAZEN_GRACZ);
  int pomoc=0;
  PunktyEnergii(&pomoc, id);
  sm.PZ-=obrazenia;
  Stab[nr]=sm;
  if(sm.PZ<=0)
  {
    Stab[nr]=Stab[S-1];
    S--;
    SOtab[sm.pozycja.x*KolumnyWMapie(NULL)+sm.pozycja.y]=0;
    liczbaSmokow(&S);
    przywrocPoleTerenu(sm.pozycja,KolumnyWMapie(NULL));
  }
  deaktywujPrzyciskAtaku(NULL);
  wyswietlStatystykiGraczaWrazZObrazeniami(obrazenia);
  return;
}

void graczAtakujeGracza(char idAtakujacy, char idBroniacy)
{
  //funkcja wywoływana przez atakGracza, jeśli ta wykryje na polu atakowanym
  //przez gracza innego gracza
  int PE=PunktyEnergii(NULL,idAtakujacy);
  int obrazenia=PE*(rand()%ZMIENNY_MNOZNIK_OBRAZEN_GRACZ+STALY_MNOZNIK_OBRAZEN_GRACZ);
  int PZ=PunktZycia(NULL,idBroniacy);
  PZ-=obrazenia;
  PunktZycia(&PZ,idBroniacy);
  PE=0;
  PunktyEnergii(&PE,idAtakujacy);
  deaktywujPrzyciskAtaku(NULL);
  wyswietlStatystykiGraczaWrazZObrazeniami(obrazenia);
  return;
}

void atakGracza(char id, Pozycja kierunekAtaku)
{
  //funkcja wywoływana po kliknięciu kombinacji klawiszy "Atak" + strzałka
  //ma za zadanie wykryć czy na atakowanym polu ktoś stoi, a jeśli tak to kto
  Pozycja graczJa = pozycjaGracza(NULL,id);
  char twid=(id=='A'?'B':'A');
  Pozycja graczTy = pozycjaGracza(NULL,twid);

  if(graczJa.x+kierunekAtaku.x==graczTy.x &&
     graczJa.y+kierunekAtaku.y==graczTy.y)
  {
    graczAtakujeGracza(id,twid);
    return;
  }
  Smok *Stab=TablicaZeSmokami(NULL);
  int S=liczbaSmokow(NULL);
  int *SOtab=TablicaObecnosciSmokow(NULL);
  int nr;
  Pozycja atak;
  atak.x=graczJa.x+kierunekAtaku.x;
  atak.y=graczJa.y+kierunekAtaku.y;
  Smok sm=znajdzSmokaNaDanymPolu(atak,&nr);
  zaatakujSmoka(id,sm,nr,Stab,SOtab,S);
  return;
}

