#ifndef GENERATOR_NAGLOWEK_H_INCLUDED
#define GENERATOR_NAGLOWEK_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>

#define DLUGOSC_NAZWY_SCIEZKI 100
#define LICZBA_SYMBOLI 7
#define LICZBA_PROB_LOSOWANIA 50

void generatorMapy(int n, int m, int S, char nazwa[]);
void stworzPodstawowaTabliceRozkladuSymboli(int szansa[]);
void stworzTabliceSymboli(char symbole[]);
void wypiszTablice(int n, int m, int tab[n][m]);


#include "kolejka.h"
#include "kontroler_naglowek.h"

void zapiszSmokiDoPliku(Smok Stab[], int S, char nazwa[]);

#endif //GENERATOR_NAGLOWEK_H_INCLUDED
