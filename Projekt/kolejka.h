#ifndef KOLEJKA_H_INCLUDED
#define KOLEJKA_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "kontroler_naglowek.h"

/*
===============================
Lista 6, Zadanie 1
Kuba Nowak
Kolejka
===============================
*/

struct kolejka
{
   Pozycja *tab;
   int rozmiar;
   Pozycja *pocz;
   Pozycja *kon;
};

typedef struct kolejka Kolejka;

Kolejka stworzKolejke(int rozmiarPoczatkowy);
Kolejka dodajElementDoKolejki(Kolejka q, Pozycja a);
Kolejka usunElement(Kolejka q, Pozycja *d);
void usunKolejke(Kolejka q);
int czyKolejkaPusta(Kolejka q);

#endif //KOLEJKA_H_INCLUDED
