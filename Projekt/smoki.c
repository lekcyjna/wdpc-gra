#include "smoki_naglowek.h"

#define LICZBA_POWTORZEN_PROB_RUCHU_SMOKA 10
#define MODULO_ZMIENNA_LICZBA_OBRAZEN_SMOKI 3
#define STALY_MNOZNIK_OBRAZEN_SMOKI 3

static int czyObokSmokaJestGracz(Smok sm, Pozycja graczA, Pozycja graczB)
{
  //sprawdzanie czy smok sąsiaduje z graczem
  int p1[]={1,-1,0,0};
  int p2[]={0,0,1,-1};
  for(int i=0;i<4;i++)
  {
    if(sm.pozycja.x+p1[i]==graczA.x && sm.pozycja.y+p2[i]==graczA.y)
    {
      return 1;
    }
    if(sm.pozycja.x+p1[i]==graczB.x && sm.pozycja.y+p2[i]==graczB.y)
    {
      return 2;
    }
  }
  return 0;
}

static int czySmokMozeWejscNaPole(int m, int nx, int ny, int tabMap[],
                           Pozycja graczA, Pozycja graczB, int SOtab[],
                           Smok sm, int energia[LICZBA_SYMBOLI])
{
  //sprawdzanie czy smok może wejść na pole wylosowane przez funkcję
  //poruszSmokiem
  int pomoc1=tabMap[(nx*m)+ny];
  //jeśli wieża lub blokada lub barykada
  if(SOtab[nx*m+ny]==1)
  {
    return 0;
  }
  if(pomoc1==0 || pomoc1==5 || pomoc1==6)
  {
    return 0;
  }
  if((nx==graczA.x && ny==graczA.y) || (nx==graczB.x && ny==graczB.y))
  {
    return 0;
  }
  if(sm.PE<energia[pomoc1])
  {
    return 0;
  }
  return 1;
}

static void poruszSmokiem(int m, int tabMap[], Smok *sm, Pozycja graczA, Pozycja graczB,
                   int SOtab[], int energia[LICZBA_SYMBOLI])
{
  //funkcja próbująca poruszyć się smokiem w jeden z 4 kierunków
  int p1[]={1,-1,0,0};
  int p2[]={0,0,1,-1};
  for(int i=0;i<LICZBA_PROB_LOSOWANIA;i++)
  {
    int p=rand()%100;
    int nx, ny;
    if(p>=0 && p<25)
    {
      nx=sm->pozycja.x+p1[0], ny=sm->pozycja.y+p2[0];
    }
    if(p>=25 && p<50)
    {
      nx=sm->pozycja.x+p1[1], ny=sm->pozycja.y+p2[1];
    }
    if(p>=50 && p<75)
    {
      nx=sm->pozycja.x+p1[2], ny=sm->pozycja.y+p2[2];
    }
    if(p>=75 && p<100)
    {
      nx=sm->pozycja.x+p1[3], ny=sm->pozycja.y+p2[3];
    }
    if(czySmokMozeWejscNaPole(m,nx,ny,tabMap,graczA,graczB,SOtab,*sm,
                              energia)==1)
    {
      przywrocPoleTerenu(sm->pozycja,m);
      SOtab[sm->pozycja.x*m+sm->pozycja.y]=0;
      SOtab[nx*m+ny]=1;
      sm->pozycja.x=nx;
      sm->pozycja.y=ny;
      sm->PE-=energia[tabMap[nx*m+ny]];
      ustawSmokaNaMapie(sm->pozycja);
      break;
    }
  }
  return;
}

static void resetujEnergieSmoka(Smok *sm)
{
  //przywrócenie smokowi jego kasymalnej energi
  sm->PE=sm->maxPE;
  return;
}

static void zaatakujGracza(Smok *sm, char idGracza)
{
  //funkcja generująca liczbę obrażeń i zadająca te obrażenia
  //graczowi po ataku smoka
  int obrazenia=sm->PE*(rand()%MODULO_ZMIENNA_LICZBA_OBRAZEN_SMOKI+STALY_MNOZNIK_OBRAZEN_SMOKI);
  sm->PE=0;
  int PZ=PunktZycia(NULL,idGracza);
  PZ-=obrazenia;
  PunktZycia(&PZ,idGracza);
  wyswietlStatystykiGracza();
  return;
}

void ruchySmokow()
{
  //podstawowa funkcja dla smoków, iteruje po wszystkich smokach
  //jeśeli smok nie sąsiaduje z graczem to rusza się nim
  //o jedno pole w losowym kierunku wywołując poruszSmokiem,
  //a jeśli sąsiaduje to atakuje gracza, dla każdego smoka
  //próbuje poruszyć się nim LICZBA_POWTORZEN_PROB_RUCHU_SMOKA razy,
  //zanim przejdzie do kolejnego

//  int nasiono=time(NULL);
//  srand(nasiono);
  int energia[LICZBA_SYMBOLI];
  stworzTabliceEnergiiDoWejsciaNaPola(energia);
  Pozycja graczA=pozycjaGracza(NULL,'A'), graczB=pozycjaGracza(NULL,'B');
  int *tabMapa=MapaWTablicy(NULL);
  int S=liczbaSmokow(NULL);
  Smok *Stab=TablicaZeSmokami(NULL);
  int *SOtab=TablicaObecnosciSmokow(NULL);
  int m=KolumnyWMapie(NULL);
  for(int i=0;i<S;i++)
  {
    resetujEnergieSmoka(&Stab[i]);
  }
  for(int i=0;i<S;i++)
  {
    for(int j=0;j<LICZBA_POWTORZEN_PROB_RUCHU_SMOKA;j++)
    {
      int pomocnicza=czyObokSmokaJestGracz(Stab[i],graczA,graczB);
      if(pomocnicza==1)
      {
        zaatakujGracza(&Stab[i],'A');
//        fprintf(stderr,"Atak na gracza A! Pozycja A:%d %d Pozycja moja:%d %d\n",
//                graczA.x,graczA.y,Stab[i].pozycja.x,Stab[i].pozycja.y);
        break;
      }
      if(pomocnicza==2)
      {
        zaatakujGracza(&Stab[i],'B');
//        fprintf(stderr,"Atak na gracza B! Pozycja B:%d %d Pozycja moja:%d %d\n",
//                graczB.x,graczB.y,Stab[i].pozycja.x,Stab[i].pozycja.y);
        break;
      }
      poruszSmokiem(m,tabMapa,&(Stab[i]),graczA,graczB,SOtab,energia);
    }
  }
  return;
}

